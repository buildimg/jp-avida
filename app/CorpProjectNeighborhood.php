<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CorpProjectNeighborhood extends Model
{
    protected $connection = 'mysql_corp';
    protected $table = 'neighborhoods';
    public $timestamps = false;

    public function neighborhood_attractions ()
    {
        return $this->hasMany(CorpProjectNeighborhoodAttraction::class, 'nh_ID', 'nh_ID');
    }
}
