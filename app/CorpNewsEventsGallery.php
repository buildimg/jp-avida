<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CorpNewsEventsGallery extends Model
{
    protected $connection = 'mysql_corp';
    protected $table = 'articles_gallery';
    public $timestamps = false;
}
