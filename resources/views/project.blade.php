@extends('layouts.main')
@section('styles')
    <link rel="stylesheet" href="{{ asset('photoswipe/photoswipe.css') }}"> 
    <link rel="stylesheet" href="{{ asset('photoswipe/default-skin/default-skin.css') }}"> 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.0/css/swiper.min.css"> 

    <!-- fotorama.css -->
    <link  href="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
    <style>
        html,body{
            height: 100%;
        }
        .search-bar {
            display: none;
            position: absolute;
            top: 81px;
            left: 0;
            width: 100%;
        }
    </style>
@endsection

@section('content')
    {{--  <pre>{{ json_encode($JPProject, JSON_PRETTY_PRINT)}}</pre>  --}}

<!-- ************************* PROJECT BREADCRUMBS ************************* -->
<div class="project-bread-crumbs navbar-fixed-top hidden-xs t-ease-fast">
    <div class="container-fluid">
        <div class="clearfix">
            <ul class="list-inline pull-left m-b-0">
                <li><a href="{{ route('home') }}">ホームページ</a></li>
                <li><i class="fa fa-angle-right text-white"></i></li>
                <li>
                    @if (($CorpProject->project_type == 'Condominium'))
                        <a href="{{ route('condominium') }}">コンドミニアム</a>
                    @elseif(($CorpProject->project_type == 'House-Lot'))
                        <a href="{{ route('houseandlot') }}">House &amp; Lot</a>
                    @elseif(($CorpProject->project_type == 'Office'))
                        <a href="{{ route('office') }}">オフィス</a>
                    @endif
                </li>
                <li><i class="fa fa-angle-right text-white"></i></li>
                <li>
                    <a href="#">
                        <span>
                            @if ($CorpProject->project_province == 'Metro Manila')
                                {{ $CorpProject->project_cities }}
                            @else
                                {{ $CorpProject->project_cities.', '.$CorpProject->project_province }}
                            @endif
                        </span>
                    </a>
                </li>
                <li><i class="fa fa-angle-right text-white"></i></li>
                <li><a href="#">{{ $JPProject->project_name }}</a></li>
            </ul>
            <div class="pull-right">
                <span class="try">Search other properties</span> <a class="c-pointer show-reb-bar">Click here.</a>
            </div>
        </div>
    </div>
</div>

<!-- ************************* PROJECT BANNER ************************* -->
<div class="project-banner">
    <img src="{{ env('AVIDA_URL_PROJECT_UPLOADS') . $CorpProject->project_upload_loc . $CorpProject->project_feat_img }}" class="w-100">
    <div class="project-heading" id="projectConcept">
        <div class="container">
            <h1>{{  $JPProject->project_name }}</h1>
            <hr class="m-y-3">
            <div class="project-details">
                <div class="clearfix {{ $CorpProject->project_status != 'Pre-Selling' ? '' : 'make-5-col'}}">
                    <div class="col-5">
                        <h4 class="m-b-1 text-uppercase">場所</h4>
                        <span class="text-muted small">
                            @if($CorpProject->project_province == 'Metro Manila')
                                {{ $CorpProject->project_cities }}
                            @else
                                {{ $CorpProject->project_cities.', '.$CorpProject->project_province }}
                            @endif
                        </span>
                    </div>

                    @if($CorpProject->project_unit_min != null && $CorpProject->project_unit_max != null)
                        <div class="col-5">
                            <h4 class="m-b-1 text-uppercase">ユニット面積</h4>
                            <span class="text-muted small">{{ $CorpProject->project_unit_min }} - {{ $CorpProject->project_unit_max }} sq. m.</span>
                        </div>
                        <div class="col-5">
                            <h4 class="m-b-1 text-uppercase">価格帯</h4>
                            <span class="text-muted small">{{ $CorpProject->project_price_min }}M - P{{ $CorpProject->project_price_max }}M</span>
                        </div>
                    @endif

                    @if($CorpProject->project_status != null)
                        <div class="col-5">
                            <h4 class="m-b-1 text-uppercase">Status:</h4>
                            <span class="text-muted small">
                                    @if($CorpProject->project_status == "RFO")
                                        Ready for Occupancy
                                    @else
                                        {{ $CorpProject->project_status }}
                                    @endif
                            </span>
                        </div>
                    @endif
                
                    @if($CorpProject->project_turnoverdate != null)
                        <div class="col-5">
                            <h4 class="m-b-1 text-uppercase">竣工日</h4>
                            <span class="text-muted small">
                                <?php
                                    $date = strtotime($CorpProject->project_turnoverdate."-01");
                                    $date = date("M Y",$date);
                                ?>
                                {{ $date }}
                            </span>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@if($CorpProject->concept->concept_status == 'Active')
    <div class="project-content">
        <div class="container">
            <h2 class="h5 m-b-1 text-red text-uppercase">マスタープランとコンセプト</h2>
            <h3 class="m-t-0 h2"><strong>{{ $JPProject ? $JPProject->concept->concept_tagline : $CorpProject->concept->concept_tagline }}</strong></h3>
            <p>{!! $JPProject ? $JPProject->concept->concept_text : $CorpProject->concept->concept_text !!}</p>
            @if($CorpProject->concept->concept_gallery->count())
                <div class="my-gallery" itemscope itemtype="http://schema.org/ImageGallery" style="display: inline-block;">
                    @foreach($CorpProject->concept->concept_gallery->where('mpgallery_img_type', 'masterplan') as $concept_gallery)
                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" class="{{ $loop->first ? '' : 'hidden' }}" style="display: inline;">
                            @php
                                $w = Image::make(env('AVIDA_URL_PROJECT_UPLOADS').$CorpProject->project_upload_loc.$concept_gallery->mpgallery_img)->width();
                                $h = Image::make(env('AVIDA_URL_PROJECT_UPLOADS').$CorpProject->project_upload_loc.$concept_gallery->mpgallery_img)->height();
                            @endphp
                            <a class="btn btn-red btn-lg p-x-4 m-r-2 m-y-3" href="{{ env('AVIDA_URL_PROJECT_UPLOADS').$CorpProject->project_upload_loc.$concept_gallery->mpgallery_img }}" itemprop="contentUrl" data-size="{{ $w.'x'.$h }}">
                                <img class="hidden" src="#" itemprop="thumbnail" />
                                View Masterplan
                            </a>
                            <figcaption itemprop="caption description" class="hidden">{{ $concept_gallery->mpgallery_img_alt }}</figcaption>
                        </figure>
                    @endforeach
                </div>
                <div class="my-gallery" itemscope itemtype="http://schema.org/ImageGallery" style="display: inline-block;">
                    @foreach($CorpProject->concept->concept_gallery->where('mpgallery_img_type', 'misc') as $concept_gallery)
                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" class="{{ $loop->first ? '' : 'hidden' }}" style="display: inline;">
                            @php
                                $w = Image::make(env('AVIDA_URL_PROJECT_UPLOADS').$CorpProject->project_upload_loc.$concept_gallery->mpgallery_img)->width();
                                $h = Image::make(env('AVIDA_URL_PROJECT_UPLOADS').$CorpProject->project_upload_loc.$concept_gallery->mpgallery_img)->height();
                            @endphp
                            <a class="btn btn-red btn-lg p-x-4" href="{{ env('AVIDA_URL_PROJECT_UPLOADS').$CorpProject->project_upload_loc.$concept_gallery->mpgallery_img }}" itemprop="contentUrl" data-size="{{ $w.'x'.$h }}">
                                <img class="hidden" src="#" itemprop="thumbnail" />
                                View Image/s
                            </a>
                            <figcaption itemprop="caption description" class="hidden">{{ $concept_gallery->mpgallery_img_alt }}</figcaption>
                        </figure>
                    @endforeach
                </div>
            @endif
            <hr>
        </div>
    </div>
@endif

<!-- ************************* PROJECT LOCATION ************************* -->
@if($CorpProject->location->location_status == 'Active')
    <div class="project-location" id="projectLocation">
        <div class="container">
            <h2 class="h5 m-b-1 text-red text-uppercase">場所</h2>
            <h3 class="m-t-0 h2"><strong>{{ $CorpProject->project_address }}</strong></h3>
            <p class="text-muted small m-b-6 text-uppercase">
                <span>
                    @if ($CorpProject->project_province == 'Metro Manila')
                        {{ $CorpProject->project_cities }}
                    @else
                        {{ $CorpProject->project_cities . ', ' . $CorpProject->project_province }}
                    @endif
                </span>
            </p>
            <p>{!! $JPProject ? $JPProject->location->content : $CorpProject->location->location_text !!}</p>
        </div>
    </div>
    <div class="project-map m-t-4">
        <div id="map_canvas" lat="{{ $CorpProject->location->location_latitude }}" long="{{ $CorpProject->location->location_longitude }}" data-radius="{{ $CorpProject->location->location_prox_radius }}" data-zoom="{{ $CorpProject->location->location_zoom_level }}"></div>
        <div class="project-map-body">
            <div class="dropdown">
              <button class="btn btn-red btn-red text-uppercase dropdown-toggle" type="button" id="btn-map-nearby" type="school" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <i class="fa fa-map-marker m-r-2"></i>Nearby Establishments
                <i class="fa fa-angle-down fa-lg t-ease m-l-2"></i>
              </button>
                <ul class="dropdown-menu dropdown-nearby-type">
                    <li><a href="#" type="school">Schools</a></li>
                    <li><a href="#" type="shopping_mall">Shopping Malls</a></li>
                    <li><a href="#" type="church">Churches</a></li>
                    <li><a href="#" type="bar">Bars</a></li>
                    <li><a href="#" type="park">Parks</a></li>
                    <li><a href="#" type="gym">Gym</a></li>
                    <li><a href="#" type="police">Police Stations</a></li>
                    <li><a href="#" type="restaurant">Restaurants</a></li>
                    <li><a href="#" type="hospital">Hospitals</a></li>
                    <li><a href="#" type="bank">Banks</a></li>
                    <li><a href="#" type="gas_station">Gas stations</a></li>
                    <li><div class="btn-clear-marker c-pointer"><u>Clear Marker</u></div></li>
                </ul>
            </div>
        </div>
    </div>
@endif

<!-- ************************* PROJECT UNIT ************************* -->
@if($CorpProject->fp_overall || $CorpProject->fp_unit)
    <div class="project-floor-plan" id="projectFloorPlan">
        <div class="container">
            <div class="row aaa">
                <div class="col-md-4 bbb">
                    <h3 class="m-t-0 text-uppercase">ユニットと間取図</h3>
                    <div class="dropdown dropdown-block visible-xs visible-sm">
                        <button class="btn btn-default btn-lg btn-block dropdown-toggle text-explore-floor-plan" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            Explore Floor Plans
                        </button>

                        <ul class="dropdown-menu list-group" id="floorPlanList">
                                @if($CorpProject->fp_overall)
                                    @if($CorpProject->fp_overall->FPoverall_status == 'Active')
                                        @if($CorpProject->fp_overall->fp_overall_images == true)
                                            <li class="h4 p-a-3 p-x-3 m-y-0">
                                                <strong>
                                                    @if ($CorpProject->project_type == 'House-Lot') 
                                                        Model Homes
                                                    @elseif ($CorpProject->project_type == 'Condominium' || $CorpProject->project_type == 'Office') 
                                                        Overall Floor Plans
                                                    @endif
                                                </strong>
                                            </li>
                                        @endif

                                        @if($CorpProject->fp_overall->fp_overall_images == true)
                                            @foreach ($CorpProject->fp_overall->fp_overall_images as $key => $item)
                                                <?php
                                                    $fp_overall_content = '';
                                                    if ($JPProject->fp_overall)
                                                    {
                                                        if ($JPProject->fp_overall->unit_images)
                                                        {
                                                            $fp_overall_content = $JPProject->fp_overall->unit_images->firstWhere('corp_overall_id', $item->overall_ID);
                                                        }
                                                    }
                                                ?>
                                                @if ($item->overall_display_status != 'hidden')
                                                    {{--  {{ json_encode($fp_overall_content, JSON_PRETTY_PRINT) }}  --}}
                                                    <a class="list-group-item c-pointer" show-tab="#overallFP{{ $key }}">
                                                        @if ($CorpProject->project_type == 'House-Lot')
                                                            <h4 class="list-group-item-heading m-b-0">
                                                                {{ $fp_overall_content ? $fp_overall_content->title : $item->overall_title }}
                                                                @if(!empty($item->overall_subtitle))
                                                                    $item->overall_subtitle
                                                                @endif
                                                            </h4>
                                                            @if(!empty($item->overall_usable_area))
                                                                <p class="list-group-item-text text-muted m-t-1">
                                                                    Floor Area: {{ $item->overall_usable_area }} sq. m.
                                                                </p>
                                                            @endif
                                                            @if(!empty($item['overall_min_lotarea']))
                                                                <p class="list-group-item-text text-muted m-t-1">
                                                                    Lot Area: {{ $item->overall_min_lotarea }} sq. m.
                                                                </p>
                                                            @endif
                                                        @else
                                                            <h4 class="list-group-item-heading m-b-0">{{ $fp_overall_content ? $fp_overall_content->title : $item->overall_title }}</h4>
                                                            @if(!empty($item->overall_subtitle))
                                                                <p class="list-group-item-text text-muted m-t-1">{{ $item->overall_subtitle }}</p>
                                                            @endif
                                                        @endif
                                                    </a>
                                                @endif
                                            @endforeach
                                            <li class="divider"></li>
                                        @endif
                                    @endif
                                @endif


                                @if($CorpProject->fp_unit)
                                    @if($CorpProject->fp_unit->FPunits_status == 'Active')
                                        @if($CorpProject->fp_unit->unit_images == true)
                                            <li class="h4 p-a-3 p-x-3 m-y-0">
                                                <strong>Unit Floor Plans</strong>
                                            </li>
                                        @endif

                                        @if($CorpProject->fp_unit->unit_images == true)
                                            @foreach ($CorpProject->fp_unit->unit_images as $key => $item)
                                                @if ($item->units_display_status != 'hidden')
                                                    <?php
                                                        $fp_unit_content = '';
                                                        if ($JPProject->fp_unit)
                                                        {
                                                            if ($JPProject->fp_unit->unit_images)
                                                            {
                                                                $fp_unit_content = $JPProject->fp_unit->unit_images->firstWhere('corp_unit_id', $item->units_ID);
                                                            }
                                                        }
                                                    ?>
                                                    <a class="list-group-item c-pointer" show-tab="#unitsFP{{ $key }}">
                                                        <h4 class="list-group-item-heading m-b-0">
                                                            {{ $fp_unit_content ? $fp_unit_content->title : $item->units_title }}
                                                            @if(!empty($item->units_subtitle))
                                                                {{ " - ".$item->units_subtitle }}
                                                            @endif
                                                        </h4>
                                                        <p class="list-group-item-text text-muted m-t-1">{{ $item->units_floor_area }} sq.m</p>
                                                    </a>
                                                @endif
                                            @endforeach
                                        @endif
                                    @endif
                                @endif
                        </ul>
                    </div>
                    <div class="hidden-xs hidden-sm">
                        <div class="list-group-floorplan" style="border: 1px solid #ddd;">
                            <ul class="list-unstyled list-group m-b-0" id="floorPlanList">
                                @if($CorpProject->fp_overall)
                                    @if($CorpProject->fp_overall->FPoverall_status == 'Active')
                                        @if($CorpProject->fp_overall->fp_overall_images == true)
                                            <li class="h4 p-a-3 p-x-3 m-y-0">
                                                <strong>
                                                    @if ($CorpProject->project_type == 'House-Lot') 
                                                        Model Homes
                                                    @elseif ($CorpProject->project_type == 'Condominium' || $CorpProject->project_type == 'Office') 
                                                        Overall Floor Plans
                                                    @endif
                                                </strong>
                                            </li>
                                        @endif

                                        @if($CorpProject->fp_overall->fp_overall_images == true)
                                            @foreach ($CorpProject->fp_overall->fp_overall_images as $key => $item)
                                                <?php
                                                    $fp_overall_content = '';
                                                    if ($JPProject->fp_overall)
                                                    {
                                                        if ($JPProject->fp_overall->unit_images)
                                                        {
                                                            $fp_overall_content = $JPProject->fp_overall->unit_images->firstWhere('corp_overall_id', $item->overall_ID);
                                                        }
                                                    }
                                                ?>
                                                @if ($item->overall_display_status != 'hidden')
                                                    {{--  {{ json_encode($fp_overall_content, JSON_PRETTY_PRINT) }}  --}}
                                                    <a class="list-group-item c-pointer" show-tab="#overallFP{{ $key }}">
                                                        @if ($CorpProject->project_type == 'House-Lot')
                                                            <h4 class="list-group-item-heading m-b-0">
                                                                {{ $fp_overall_content ? $fp_overall_content->title : $item->overall_title }}
                                                                @if(!empty($item->overall_subtitle))
                                                                    $item->overall_subtitle
                                                                @endif
                                                            </h4>
                                                            @if(!empty($item->overall_usable_area))
                                                                <p class="list-group-item-text text-muted m-t-1">
                                                                    Floor Area: {{ $item->overall_usable_area }} sq. m.
                                                                </p>
                                                            @endif
                                                            @if(!empty($item['overall_min_lotarea']))
                                                                <p class="list-group-item-text text-muted m-t-1">
                                                                    Lot Area: {{ $item->overall_min_lotarea }} sq. m.
                                                                </p>
                                                            @endif
                                                        @else
                                                            <h4 class="list-group-item-heading m-b-0">{{ $fp_overall_content ? $fp_overall_content->title : $item->overall_title }}</h4>
                                                            @if(!empty($item->overall_subtitle))
                                                                <p class="list-group-item-text text-muted m-t-1">{{ $item->overall_subtitle }}</p>
                                                            @endif
                                                        @endif
                                                    </a>
                                                @endif
                                            @endforeach
                                            <li class="divider"></li>
                                        @endif
                                    @endif
                                @endif


                                @if($CorpProject->fp_unit)
                                    @if($CorpProject->fp_unit->FPunits_status == 'Active')
                                        @if($CorpProject->fp_unit->unit_images == true)
                                            <li class="h4 p-a-3 p-x-3 m-y-0">
                                                <strong>Unit Floor Plans</strong>
                                            </li>
                                        @endif

                                        @if($CorpProject->fp_unit->unit_images == true)
                                            @foreach ($CorpProject->fp_unit->unit_images as $key => $item)
                                                @if ($item->units_display_status != 'hidden')
                                                    <?php
                                                        $fp_unit_content = '';
                                                        if ($JPProject->fp_unit)
                                                        {
                                                            if ($JPProject->fp_unit->unit_images)
                                                            {
                                                                $fp_unit_content = $JPProject->fp_unit->unit_images->firstWhere('corp_unit_id', $item->units_ID);
                                                            }
                                                        }
                                                    ?>
                                                    <a class="list-group-item c-pointer" show-tab="#unitsFP{{ $key }}">
                                                        <h4 class="list-group-item-heading m-b-0">
                                                            {{ $fp_unit_content ? $fp_unit_content->title : $item->units_title }}
                                                            @if(!empty($item->units_subtitle))
                                                                {{ " - ".$item->units_subtitle }}
                                                            @endif
                                                        </h4>
                                                        <p class="list-group-item-text text-muted m-t-1">{{ $item->units_floor_area }} sq.m</p>
                                                    </a>
                                                @endif
                                            @endforeach
                                        @endif
                                    @endif
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-8 ccc">
                    <div class="tab-content" id="tabFlorPlan">
                        @if($CorpProject->fp_overall)
                            @if($CorpProject->fp_overall->FPoverall_status == 'Active')
                                @if($CorpProject->fp_overall->fp_overall_images == true)
                                    @foreach ($CorpProject->fp_overall->fp_overall_images as $key => $item)
                                        @if ($item->overall_display_status != 'hidden')
                                            <div role="tabpanel" class="tab-pane fade" id="overallFP{{ $key }}">
                                                <img src="{{ env('AVIDA_URL_PROJECT_UPLOADS').$CorpProject->project_upload_loc.$item->overall_image }}" alt="{{ $item->overall_title }}" class="w-100">
                                            </div>
                                        @endif
                                    @endforeach
                                @endif
                            @endif
                        @endif

                        @if($CorpProject->fp_unit)
                            @if($CorpProject->fp_unit->FPunits_status == 'Active')
                                @if($CorpProject->fp_unit->unit_images == true)
                                    @foreach ($CorpProject->fp_unit->unit_images as $key => $item)
                                        @if ($item->units_display_status != 'hidden')
                                            <div role="tabpanel" class="tab-pane fade" id="unitsFP{{ $key }}">
                                                <img src="{{ env('AVIDA_URL_PROJECT_UPLOADS').$CorpProject->project_upload_loc.$item->units_image }}" alt="{{ $item->units_title }}" class="w-100">
                                            </div>
                                        @endif
                                    @endforeach
                                @endif
                            @endif
                        @endif
                    </div>
                </div>
                
            </div>
        </div>
    </div>
@endif

<!-- ************************* AMENITIES ************************* -->
@if ($CorpProject->amenity->amenities_status == 'Active')
    <?php
        function getYouTubeIdFromURL($url){
            $url_string = parse_url($url, PHP_URL_QUERY);
            parse_str($url_string, $args);
            return isset($args['v']) ? $args['v'] : false;
        }        
    ?>
    <div class="amenities-gallery" id="amenitiesGallery">
        <div class="fotorama-wrap">
            <span class="amenities-gallery-title">施設と設備</span>
            <div class="fotorama"
                data-fit="cover"
                data-width="100%"
                data-ratio="16/9"
                data-max-width="100%"
                data-max-height="100%"
                data-nav="thumbs"
                data-thumbheight="65"
                data-thumbwidth="65">
                @foreach($CorpProject->amenity->amenity_gallery as $item)
                    @if($item->gallery_type == 'avp')
                        <a href="{{ $item->gallery_url }}" data-caption="{{ $item->gallery_title }}">
                            <img src="https://img.youtube.com/vi/{{ getYouTubeIdFromURL($item->gallery_img) }}/maxresdefault.jpg" alt="{{ $item->gallery_img_alt }}">
                        </a>
                    @endif
                @endforeach
                @foreach($CorpProject->amenity->amenity_gallery as $item)
                    @if($item->gallery_type == 'image')
                        <img src="{{ env('AVIDA_URL_PROJECT_UPLOADS') . $CorpProject->project_upload_loc.$item->gallery_img }}" alt="{{ $item->gallery_img_alt }}" data-caption="{{ $JPProject ? $JPProject->amenity->amenity_gallery->where('corp_gallery_id', $item->gallery_ID)->first()->gallery_title : $item->gallery_title }}">
                    @endif
                @endforeach
            </div>
        </div>
        @if (!empty($CorpProject->amenity->amenities_overall_text))
            <div class="facility-amenities">
                <div class="container">
                    <br>
                    <br>
                    {!! $JPProject ? $JPProject->amenity->content : $CorpProject->amenity->amenities_overall_text !!}
                </div>
            </div>
        @endif
    </div>
@endif

@if($CorpProject->constructions->const_status == 'Active')
    <div class="constructions">
        <div class="container">
            <hr>
            <h3 class="m-t-0 h2 m-b-6"><strong>最新建築状況</strong></h3>
            <div class="row my-gallery" itemscope itemtype="http://schema.org/ImageGallery">
                @foreach ($CorpProject->constructions->construction_galleries as $key => $construction)
                    <figure class="col-md-4 col-sm-6 {{ $key > 2 ? 'hidden' : '' }}" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        @php
                            $x = explode('.', $construction->const_img_image);
                            $const_img_image = $x[0].'-th'.'.jpg';
                        @endphp
                        <a href="{{ env('AVIDA_URL_PROJECT_UPLOADS').$CorpProject->project_upload_loc.$construction->const_img_image }}" itemprop="contentUrl" data-size="1200x900" class="const_img_link">
                            <img src="{{ env('AVIDA_URL_PROJECT_UPLOADS').$CorpProject->project_upload_loc.$const_img_image }}" itemprop="thumbnail" alt="{{ $construction->const_img_title }}" class="w-100" />
                            @if($CorpProject->constructions->construction_galleries->count() > 2 && $key == 2 )
                                <div class="more-number {{ $CorpProject->constructions->construction_galleries->count() < 3 ? 'hidden' : '' }}">
                                    <div class="va-block">
                                        <div class="va-middle text-center">
                                            <span>+{{ $CorpProject->constructions->construction_galleries->count() - 3 }}</span>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </a>
                        <figcaption itemprop="caption description">
                            <?php $date = strtotime($construction->const_img_image_alt."/01");
                            $date = date("F Y",$date); ?>
                            <p class="text-red text-center m-t-2"><span>As of</span> <strong>{{ $date }}</strong></p>
                        </figcaption>
                    </figure>
                @endforeach
            </div>
        </div>
    </div>
@endif

<!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

    <!-- Background of PhotoSwipe. 
         It's a separate element as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>

    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">

        <!-- Container that holds slides. 
            PhotoSwipe keeps only 3 of them in the DOM to save memory.
            Don't modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">

                <!--  Controls are self-explanatory. Order can be changed. -->

                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                <button class="pswp__button pswp__button--share" title="Share"></button>

                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                      <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                      </div>
                    </div>
                </div>
            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div> 
            </div>

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>

        </div>

    </div>

</div>
@endsection

@section('scripts')
    <script src="{{ asset('photoswipe/photoswipe.min.js') }}"></script>
    <script src="{{ asset('photoswipe/photoswipe-ui-default.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.0/js/swiper.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>

    <script>
        var initPhotoSwipeFromDOM = function(gallerySelector) {

            // parse slide data (url, title, size ...) from DOM elements 
            // (children of gallerySelector)
            var parseThumbnailElements = function(el) {
                var thumbElements = el.childNodes,
                    numNodes = thumbElements.length,
                    items = [],
                    figureEl,
                    linkEl,
                    size,
                    item;

                for(var i = 0; i < numNodes; i++) {

                    figureEl = thumbElements[i]; // <figure> element

                    // include only element nodes 
                    if(figureEl.nodeType !== 1) {
                        continue;
                    }

                    linkEl = figureEl.children[0]; // <a> element

                    size = linkEl.getAttribute('data-size').split('x');

                    // create slide object
                    item = {
                        src: linkEl.getAttribute('href'),
                        w: parseInt(size[0], 10),
                        h: parseInt(size[1], 10)
                    };



                    if(figureEl.children.length > 1) {
                        // <figcaption> content
                        item.title = figureEl.children[1].innerHTML; 
                    }

                    if(linkEl.children.length > 0) {
                        // <img> thumbnail element, retrieving thumbnail url
                        item.msrc = linkEl.children[0].getAttribute('src');
                    } 

                    item.el = figureEl; // save link to element for getThumbBoundsFn
                    items.push(item);
                }

                return items;
            };

            // find nearest parent element
            var closest = function closest(el, fn) {
                return el && ( fn(el) ? el : closest(el.parentNode, fn) );
            };

            // triggers when user clicks on thumbnail
            var onThumbnailsClick = function(e) {
                e = e || window.event;
                e.preventDefault ? e.preventDefault() : e.returnValue = false;

                var eTarget = e.target || e.srcElement;

                // find root element of slide
                var clickedListItem = closest(eTarget, function(el) {
                    return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
                });

                if(!clickedListItem) {
                    return;
                }

                // find index of clicked item by looping through all child nodes
                // alternatively, you may define index via data- attribute
                var clickedGallery = clickedListItem.parentNode,
                    childNodes = clickedListItem.parentNode.childNodes,
                    numChildNodes = childNodes.length,
                    nodeIndex = 0,
                    index;

                for (var i = 0; i < numChildNodes; i++) {
                    if(childNodes[i].nodeType !== 1) { 
                        continue; 
                    }

                    if(childNodes[i] === clickedListItem) {
                        index = nodeIndex;
                        break;
                    }
                    nodeIndex++;
                }



                if(index >= 0) {
                    // open PhotoSwipe if valid index found
                    openPhotoSwipe( index, clickedGallery );
                }
                return false;
            };

            // parse picture index and gallery index from URL (#&pid=1&gid=2)
            var photoswipeParseHash = function() {
                var hash = window.location.hash.substring(1),
                params = {};

                if(hash.length < 5) {
                    return params;
                }

                var vars = hash.split('&');
                for (var i = 0; i < vars.length; i++) {
                    if(!vars[i]) {
                        continue;
                    }
                    var pair = vars[i].split('=');  
                    if(pair.length < 2) {
                        continue;
                    }           
                    params[pair[0]] = pair[1];
                }

                if(params.gid) {
                    params.gid = parseInt(params.gid, 10);
                }

                return params;
            };

            var openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {
                var pswpElement = document.querySelectorAll('.pswp')[0],
                    gallery,
                    options,
                    items;

                items = parseThumbnailElements(galleryElement);

                // define options (if needed)
                options = {

                    // define gallery index (for URL)
                    galleryUID: galleryElement.getAttribute('data-pswp-uid'),

                    getThumbBoundsFn: function(index) {
                        // See Options -> getThumbBoundsFn section of documentation for more info
                        var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
                            pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                            rect = thumbnail.getBoundingClientRect(); 

                        return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
                    }

                };

                // PhotoSwipe opened from URL
                if(fromURL) {
                    if(options.galleryPIDs) {
                        // parse real index when custom PIDs are used 
                        // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
                        for(var j = 0; j < items.length; j++) {
                            if(items[j].pid == index) {
                                options.index = j;
                                break;
                            }
                        }
                    } else {
                        // in URL indexes start from 1
                        options.index = parseInt(index, 10) - 1;
                    }
                } else {
                    options.index = parseInt(index, 10);
                }

                // exit if index not found
                if( isNaN(options.index) ) {
                    return;
                }

                if(disableAnimation) {
                    options.showAnimationDuration = 0;
                }

                // Pass data to PhotoSwipe and initialize it
                gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
                gallery.init();
            };

            // loop through all gallery elements and bind events
            var galleryElements = document.querySelectorAll( gallerySelector );

            for(var i = 0, l = galleryElements.length; i < l; i++) {
                galleryElements[i].setAttribute('data-pswp-uid', i+1);
                galleryElements[i].onclick = onThumbnailsClick;
            }

            // Parse URL and open gallery if it contains #&pid=3&gid=1
            var hashData = photoswipeParseHash();
            if(hashData.pid && hashData.gid) {
                openPhotoSwipe( hashData.pid ,  galleryElements[ hashData.gid - 1 ], true, true );
            }
        };

        // execute above function
        initPhotoSwipeFromDOM('.my-gallery');


        // location
        var map;
        var infowindow;
        var markers = [];
        function initMap() {
            var pyrmont = {
                lat: parseFloat($('#map_canvas').attr('lat')),
                lng: parseFloat($('#map_canvas').attr('long'))
            };

            var data_radius=parseFloat($('#map_canvas').attr('data-radius'));
            var data_zoom=parseFloat($('#map_canvas').attr('data-zoom'));



            map = new google.maps.Map(document.getElementById('map_canvas'), {
                center: pyrmont,
                zoom: data_zoom,
                scrollwheel: false
            });

            var marker = new google.maps.Marker({
                position: pyrmont,
                map: map,
                icon: 'https://www.avidaland.com/heres-a-spot.png'
            });
            markers.push(marker);

            // Add circle overlay and bind to marker
            var circle = new google.maps.Circle({
                map: map,
                radius: data_radius,
                fillOpacity: 0.1,
                fillColor: '#c9302c',
                strokeColor: '#c9302c',
                strokeWeight: 2,
                strokeOpacity: 0.5
            });
            circle.bindTo('center', marker, 'position');

            infowindow = new google.maps.InfoWindow();
            var service = new google.maps.places.PlacesService(map);

            $('.dropdown-nearby-type a').click(function(e) {
                e.stopPropagation();
                e.preventDefault();

                $('.dropdown-nearby-type a').removeClass('active');
                $(this).addClass('active');

                var x = this.type;
                //console.log(x);

                deleteMarkers();
                addMarker(pyrmont);

                //remove circle
                //circle.setMap(null);

                service.nearbySearch({
                    location: pyrmont,
                    radius: data_radius,
                    type: [x]
                }, callback);

                // console.log(callback.results);

                // if(callback.results === 'undefined'){
                //     console.log('yes');
                // }else{
                //     console.log('no');
                // }

                // try {
                //     callback.results
                // } catch (err) {
                //     console.log('no');
                // }
            });

            $('#select_nearby_type').on('change', function(e){
                deleteMarkers();
                addMarker(pyrmont);

                service.nearbySearch({
                    location: pyrmont,
                    radius: 1500,
                    type: [e.target.value]
                }, callback);
            });


            $('.btn-clear-marker').click(function(){
                deleteMarkers();
                addMarker(pyrmont);
                $('.dropdown-nearby-type a').removeClass('active');
            });
        }

        function callback(results, status) {
            if (status === google.maps.places.PlacesServiceStatus.OK) {
                for (var i = 0; i < results.length; i++) {
                    createMarker(results[i]);
                    console.log(results);
                }
            }
        }

        function createMarker(place) {
            var placeLoc = place.geometry.location;
            var marker = new google.maps.Marker({
                map: map,
                icon: 'https://www.avidaland.com/heres-a-spot.png',
                position: place.geometry.location
            });

            markers.push(marker);

            google.maps.event.addListener(marker, 'click', function() {
                infowindow.setContent(place.name);
                infowindow.open(map, this);
            });
        }

        // Adds a marker to the map and push to the array.
        function addMarker(location) {
            var marker = new google.maps.Marker({
                position: location,
                map: map,
                icon: 'https://www.avidaland.com/heres-a-spot.png'
            });
            markers.push(marker);
        }

        // Sets the map on all markers in the array.
        function setMapOnAll(map) {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
            }
        }

        // Removes the markers from the map, but keeps them in the array.
        function clearMarkers() {
            setMapOnAll(null);
        }
        // Shows any markers currently in the array.
        function showMarkers() {
            setMapOnAll(map);
        }

        // Deletes all markers in the array by removing references to them.
        function deleteMarkers() {
            clearMarkers();
            markers = [];
        }

        //florPlan

        function activaTab() {
            $('#tabFlorPlan .tab-pane').removeClass('active in');
        }
        function removeTabFloorPlanUnit() {
            $('#tabFloorPlanUnit .tab-pane').removeClass('active in');
        }

        var florPlan = $('#floorPlanList a.list-group-item');
        var florPlanUnit = $('#floorPlanListUnit a.list-group-item');

        florPlan.eq(0).addClass('active');
        $('.list-group-floorplan a.list-group-item').eq(0).addClass('active');
        florPlanUnit.eq(0).addClass('active');

        // get selected floor plan text 
        $('.text-explore-floor-plan').text($('#floorPlanList a.list-group-item').eq(0).text());
        $('.text-unit-floor-plan').text($('#floorPlanList a.list-group-item').eq(0).text());

        $('#tabFlorPlan .tab-pane').eq(0).addClass('active in');
        $('#tabFloorPlanUnit .tab-pane').eq(0).addClass('active in');

        florPlan.on('click', function() {
            var show = $(this).attr('show-tab'),
                text = $(this).text();
                
            $('.text-explore-floor-plan').text(text);
            activaTab();
            florPlan.removeClass('active');
            $(this).addClass('active');

            // code delay 'in' class 
            $(show).addClass('active').delay(0).queue(function() {
                $(window).trigger('scroll');
                $(this).addClass("in").dequeue();
            });

            $('.dropdown-toggle').parent().toggleClass('open');
        });

        florPlanUnit.on('click', function() {
            var show = $(this).attr('show-tab'),
                text = $(this).text();
                
            $('.text-unit-floor-plan').text(text);

            removeTabFloorPlanUnit();

            florPlanUnit.removeClass('active');
            $(this).addClass('active');

            // code delay 'in' class 
            $(show).addClass('active').delay(0).queue(function() {
                $(window).trigger('scroll');
                $(this).addClass("in").dequeue();
            });
            $('.dropdown-toggle').parent().toggleClass('open');
        });
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAuGDFxjpRyTCgH_DrxMZmSQ5s8WYfvwEU&libraries=places&callback=initMap" async defer></script>
@endsection