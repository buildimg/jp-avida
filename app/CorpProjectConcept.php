<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CorpProjectConcept extends Model
{
    protected $connection = 'mysql_corp';
    protected $table = 'project_concept';
    public $timestamps = false;

    public function concept_gallery ()
    {
        return $this->hasMany(CorpProjectConceptGallery::class, 'concept_ID', 'concept_ID');
    }
}
