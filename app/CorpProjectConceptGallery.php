<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CorpProjectConceptGallery extends Model
{
    protected $connection = 'mysql_corp';
    protected $table = 'project_concept_mpgallery';
    public $timestamps = false;
}
