<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StaticController extends Controller
{
    public function brandhistory()
    {
        return view('brand_history');
    }

    public function missionandvision()
    {
    	return view('mission_and_vision');
    }

    public function contactus()
    {
    	return view('contact_us');
    }

    public function privacypolicy()
    {
    	return view('privacy_policy');
    }

    public function termsandconditions()
    {
    	return view('terms_and_conditions');
    }
}