<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JPProjectAmenityGallery extends Model
{
    protected $table = 'jp_project_amenity_galleries';
}
