<div class="box-data-holder overlay-wrapper">
	<div class="overlay hidden"><i class="fa fa-spin fa-refresh"></i></div>
	<div class="box-body">
	    <table class="table table-bordered">
		  	<tr>
				<th width="50">Featured</th>
				<th>Project Name</th>
				<th>Type</th>
				<th>Project Status</th>
				<th>Status</th>
				<th>Japanese</th>
				<th>Actions</th>
		  	</tr>
		  	@foreach($Projects as $Proj)
		  		<?php
					$selected_project = $JPProject->firstWhere('corp_project_id', $Proj->project_ID);        			
		  		?>
		      	<tr>
					<td class="text-center">
						{{-- <i class="fa fa-lg fa-{{ $Proj->project_featured == 'Yes' ? 'star text-danger' : 'star-o' }} "></i> --}}
						<label class="custom-control custom-checkbox">
		                    <input type="checkbox" class="custom-control-input" {{($selected_project ? '' : 'disabled')}} data-id="{{ $Proj->project_ID }}" {{ ($selected_project ?  ($selected_project->featured == 1 ? 'checked' : '') : '')}}>
		                    <span class="custom-control-indicator"></span>
		                </label>
					</td>
					<td>  {{ $Proj->project_name }} <br><span class="text-muted"><strong>Address:</strong> {{ $Proj->project_address }}</span></td>
					<td>
		                <label class="label {{ App\CorpProject::PROJECT_TYPE[$Proj->project_type] }}">
		                    {{ $Proj->project_type }}
		                </label>
					</td>
					<td>
		                <label class="label {{ App\CorpProject::PROJECT_STATUS[$Proj->project_status] }}">
		                    {{ $Proj->project_status }}
		                </label>
					</td>
					<td>{{ $Proj->project_overall_status }}</td>
					<td><button class="btn {{($selected_project ? 'btn-danger' : 'btn-default')}} btn-flat btn-block js-mark_project_toggle" data-id="{{ $Proj->project_ID }}">{{($selected_project ? 'Unmark' : 'Mark')}}</button></td>
					
					<td>
		                <div class="btn-group btn-block">
		                    <button type="button" {{($selected_project ? '' : 'disabled')}} class="btn btn-default btn-flat btn-block dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		                        Action <span class="caret"></span>
		                    </button>
		                    <ul class="dropdown-menu dropdown-menu-right dropdown-actions">
		                        <li><a href="#" data-toggle="modal" data-target="#modal-default" data-project-id="{{ $Proj->project_ID }}" data-tab-id="5"><i class="fa fa-pencil text-red"></i> Project Concept</a></li>
		                        <li><a href="#" data-toggle="modal" data-target="#modal-default" data-project-id="{{ $Proj->project_ID }}" data-tab-id="1"><i class="fa fa-map-marker text-red"></i> Location</a></li>
		                        <li><a href="#" data-toggle="modal" data-target="#modal-default" data-project-id="{{ $Proj->project_ID }}" data-tab-id="2"><i class="fa fa-picture-o text-red"></i> Overall Floor Plan Unit</a></li>
		                        <li><a href="#" data-toggle="modal" data-target="#modal-default" data-project-id="{{ $Proj->project_ID }}" data-tab-id="3"><i class="fa fa-picture-o text-red"></i> Floor Plan Unit Details</a></li>
		                        <li><a href="#" data-toggle="modal" data-target="#modal-default" data-project-id="{{ $Proj->project_ID }}" data-tab-id="4"><i class="fa fa-picture-o text-red"></i> Amenities & Gallery</a></li>
		                        <li><a href="#" data-toggle="modal" data-target="#modal-default" data-project-id="{{ $Proj->project_ID }}" data-tab-id="6"><i class="fa fa-pencil text-red"></i> Construction Details</a></li>
		                        <li class="divider"></li>
		                        <li><a href="#" data-toggle="modal" data-target="#modal-default" data-project-id="{{ $Proj->project_ID }}" data-id="tab1"><i class="fa fa-edit text-red"></i> Disable</a></li>
		                    </ul>
		                </div>
					</td>
		      	</tr>
		  	@endforeach
		</table>
    </div>
	<!-- /.box-body -->
	<div class="box-footer clearfix">
	   {{ $Projects->links() }}
	</div>
</div>