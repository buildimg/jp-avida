<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JPProjectFloorPlanUnit extends Model
{
    protected $table = 'jp_project_floor_plan_units';
    public function unit_images ()
    {
        return $this->hasMany(JPProjectFloorPlanUnitGallery::class, 'corp_fp_unit_id', 'corp_fp_unit_id');
    }
}
