
<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="form_project">
                {{csrf_field()}}
                <input type="hidden" name="project_id" value="{{ $JPProject ? $JPProject->id : '' }}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">
                        {{--  {{ $project_name }}  --}}
                        <div class="form-group">
                            <input class="form-control" type="text" name="project_name" value="{{ $JPProject ? ($JPProject->project_name ? $JPProject->project_name : $CorpProject->project_name ) : $CorpProject->project_name }}">
                        </div>
                    </h4>
                </div>
                <div class="modal-body">

                    {{--  <pre>{{ json_encode([$JPProject, 'corpxx' => $CorpProject], JSON_PRETTY_PRINT) }}</pre>  --}}
                    {{--  {{ $project_folder }}  --}}
                    <div class="modal__row clearfix">
                        <div class="modal__col modal__col--left">
                            <ul class="nav list-unstyled" role="tablist">
                                @foreach ($tabs as $key => $tab)
                                    <li class="{{$tab['tab'] == $tab_id ? 'active' : ''}}" ><a href="#tab{{$tab['tab']}}" role="tab" data-toggle="tab"><i class="fa {{ $tab['icon'] }}"></i> {{$tab['text']}}</a></li>
                                @endforeach
                                {{--  <li><a href="#tab2" role="tab" data-toggle="tab"><i class="fa fa-picture-o"></i> Office Photo Gallery</a></li>
                                <li><a href="#tab3" role="tab" data-toggle="tab"><i class="fa fa-pencil"></i> Office Space</a></li>
                                <li><a href="#tab4" role="tab" data-toggle="tab"><i class="fa fa-picture-o"></i> Unit Details</a></li>
                                <li><a href="#tab5" role="tab" data-toggle="tab"><i class="fa fa-pencil"></i> Building Features</a></li>
                                <li><a href="#tab6" role="tab" data-toggle="tab"><i class="fa fa-picture-o"></i> Amenities & Gallery</a></li>  --}}
                            </ul>
                        </div>
                        <div class="modal__col modal__col--right">
                            <div class="modal__col__content">
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade {{$tab_id == 1 ? 'active in' : ''}}" id="tab1">
                                        <h4>Location Content</h4>
                                        <textarea class="form-control textarea" name="location" style="height: 250px;">{{ $JPProject->location ? $JPProject->location->content : '' }}</textarea>
                                        
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade {{$tab_id == 2 ? 'active in' : ''}}" id="tab2">
                                        <h4>Unit Details</h4>
                                        @if ($JPProject->fp_overall)
                                            <textarea class="form-control textarea" name="overall_floor_plan_details" style="height: 250px;">{{ $JPProject->fp_overall ? $JPProject->fp_overall->floor_plan_text : '' }}</textarea>
                                            <input type="hidden" name="overall_floor_plan_id" value="{{ $JPProject->fp_overall->id }}">
                                            <hr>
                                            <div class="row">
                                                @if ($JPProject->fp_overall->unit_images)
                                                    @foreach($JPProject->fp_overall->unit_images as $unit_image)
                                                        <?php
                                                            $overall_plan_floor_img = NULL;
                                                            if ($CorpProject)
                                                            {
                                                                if ($CorpProject->fp_overall)
                                                                {
                                                                    if ($CorpProject->fp_overall->fp_overall_images)
                                                                    {
                                                                        $overall_plan_floor_img = $CorpProject->fp_overall->fp_overall_images->firstWhere('overall_ID', $unit_image->corp_overall_id);
                                                                    }
                                                                }
                                                            }
                                                        ?>
                                                        <div class="col-md-6">
                                                            <img src="http://avidaland.com/uploads/projects/{{ $project_folder.'/'.$overall_plan_floor_img->overall_image }}" class="img-responsive">
                                                            <input type="text" class="form-control" name="overall_floor_plan_gallery_text[]" value="{{$unit_image->title}}" /> <br>
                                                            <input type="hidden" class="form-control" name="overall_floor_plan_gallery_id[]" value="{{$unit_image->id}}" />
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                        @endif 
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade {{$tab_id == 3 ? 'active in' : ''}}" id="tab3">
                                        <h4>Floor Plan Unit Details</h4>
                                        @if ($JPProject->fp_unit)
                                            <textarea class="form-control textarea" name="unit_details" style="height: 250px;">{{ $JPProject->fp_unit ? $JPProject->fp_unit->content : '' }}</textarea>
                                            <hr>
                                            <div class="row">
                                                @if ($JPProject->fp_unit->unit_images)
                                                    @foreach($JPProject->fp_unit->unit_images as $unit_image)
                                                        <?php
                                                            $unit_img = NULL;
                                                            if ($CorpProject)
                                                            {
                                                                if ($CorpProject->fp_unit)
                                                                {
                                                                    if ($CorpProject->fp_unit->unit_images)
                                                                    {
                                                                        $unit_img = $CorpProject->fp_unit->unit_images->firstWhere('units_ID', $unit_image->corp_unit_id);
                                                                    }
                                                                }
                                                            }
                                                        ?>
                                                        <div class="col-md-6">
                                                            <img src="http://avidaland.com/uploads/projects/{{ $project_folder.'/'.$unit_img->units_image }}" class="img-responsive">
                                                            <input type="text" class="form-control" name="floor_plan_unit_gallery_text[]" value="{{$unit_image->title}}" /> <br>
                                                            <input type="hidden" class="form-control" name="floor_plan_unit_gallery_id[]" value="{{$unit_image->id}}" />
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                        @endif
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade {{$tab_id == 4 ? 'active in' : ''}}" id="tab4">
                                        <h4>Amenities & Gallery</h4>
                                        <textarea class="form-control textarea" name="amenity" style="height: 250px;">{{ $JPProject->amenity ? $JPProject->amenity->content : '' }}</textarea>
                                        <hr>
                                        <div class="row">

                                            @if ($JPProject->amenity)
                                                <?php
                                                    function getYouTubeIdFromURL($url){
                                                        $url_string = parse_url($url, PHP_URL_QUERY);
                                                        parse_str($url_string, $args);
                                                        return isset($args['v']) ? $args['v'] : false;
                                                    }        
                                                ?>
                                                @if ($JPProject->amenity->amenity_gallery)
                                                    @foreach ($JPProject->amenity->amenity_gallery as $amenity_gallery)
                                                        <?php
                                                            $amenity_img = $CorpProjectAmenityGallery->firstWhere('gallery_ID', $amenity_gallery->corp_gallery_id);
                                                        ?>
                                                        <div class="col-md-6">
                                                            @if ($amenity_img->gallery_type == 'image')
                                                                <img src="http://avidaland.com/uploads/projects/{{ $project_folder.'/'.$amenity_img->gallery_img }}" class="img-responsive">
                                                            @else                                                                
                                                                <img src="https://img.youtube.com/vi/{{ getYouTubeIdFromURL($amenity_img->gallery_img) }}/maxresdefault.jpg" class="img-responsive">
                                                            @endif
                                                            <input type="text" class="form-control" name="amenity_gallery_text[]" value="{{$amenity_gallery->gallery_title}}"> <br>
                                                            <input type="hidden" class="form-control" name="amenity_gallery_id[]" value="{{$amenity_gallery->id}}">
                                                        </div>
                                                    @endforeach
                                                @endif
                                            @endif
                                        </div>
                                    </div>

                                    
                                    <div role="tabpanel" class="tab-pane fade {{$tab_id == 5 ? 'active in' : ''}}" id="tab5">
                                        <h4>Project Concept</h4>
                                        <input type="hidden" name="concept_id" value="{{ $JPProject->concept ? $JPProject->concept->id : ''}}">
                                        <div class="form-group">
                                            <label for="">Tagline</label>
                                            <input type="text" name="concept_tagline" class="form-control" value="{{ $JPProject->concept ? $JPProject->concept->concept_tagline : '' }}">
                                        </div>

                                        <textarea class="form-control textarea" name="concept_content" style="height: 250px;">{{ $JPProject->concept ? $JPProject->concept->concept_text : '' }}</textarea>
                                    </div>
                                    
                                    <div role="tabpanel" class="tab-pane fade {{$tab_id == 6 ? 'active in' : ''}}" id="tab6">
                                        <div class="row">
                                            @if ($JPProject->constructions)
                                                <input type="hidden" name="construction_id" value="{{$JPProject->constructions->id}}">
                                                <h4>Constructions</h4>
                                                    <textarea class="form-control textarea" name="construction" style="height: 250px;">{{ $JPProject->constructions ? $JPProject->constructions->content : '' }}</textarea>
                                                <hr>
                                                @if ($JPProject->constructions->construction_gallery)
                                                    @foreach ($JPProject->constructions->construction_gallery as $construction_gallery)
                                                        <?php
                                                            $construction_img = $CorpProject->constructions->construction_galleries->firstWhere('const_img_ID', $construction_gallery->corp_const_img_id);
                                                        ?>
                                                        <div class="col-md-6">
                                                            <img src="http://avidaland.com/uploads/projects/{{ $project_folder.'/'.$construction_img->const_img_image }}" class="img-responsive">
                                                            <input type="text" class="form-control" name="construction_gallery_text[]" value="{{$construction_gallery->image_title}}"> <br>
                                                            <input type="hidden" class="form-control" name="construction_gallery_id[]" value="{{$construction_gallery->id}}">
                                                        </div>
                                                    @endforeach
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-circle-o-notch fa-spin hidden"></i> Save changes</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>