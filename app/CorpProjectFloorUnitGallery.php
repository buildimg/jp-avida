<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CorpProjectFloorUnitGallery extends Model
{
    protected $connection = 'mysql_corp';
    protected $table = 'project_fp_units_img';
    public $timestamps = false;
}
