<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index')->name('home');
Route::get('/properties','PropertiesController@properties')->name('properties');
Route::get('/house-and-lot','PropertiesController@houseandlot')->name('houseandlot');
Route::get('/condominium','PropertiesController@condominium')->name('condominium');
Route::get('/office','PropertiesController@office')->name('office');

Route::get('/brand-history','StaticController@brandhistory')->name('brandhistory');
Route::get('/mission-and-vision','StaticController@missionandvision')->name('missionandvision');
Route::get('/contact-us','StaticController@contactus')->name('contactus');
Route::get('/privacy-policy','StaticController@privacypolicy')->name('privacypolicy');
Route::get('/terms-and-conditions','StaticController@termsandconditions')->name('termsandconditions');

Auth::routes();

Route::group(['prefix' => 'cms', 'middleware' => 'auth'], function () {

	Route::group(['prefix' => 'projects'], function () {	
		Route::get('', 'Admin\ProjectController@projects')->name('cms.projects');
		Route::post('mark-project-toggle', 'Admin\ProjectController@mark_project')->name('cms.mark_project');
		Route::post('toggle-featured-project', 'Admin\ProjectController@toggle_featured_project')->name('cms.toggle_featured_project');
		Route::post('modal-data', 'Admin\ProjectController@modal_data')->name('cms.modal_data');
		Route::post('save-proeject-details', 'Admin\ProjectController@save_project_details')->name('cms.save_project_details');
	});

	Route::group(['prefix' => 'user'], function (){
		Route::post('change-profile-modal', 'Admin\ProjectController@change_profile_modal')->name('cms.change_profile_modal');
		Route::post('save-profile-data', 'Admin\ProjectController@save_profile_data')->name('cms.save_profile_data');
		
	});

});

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/{projectname?}', 'ProjectController@project')->name('project');