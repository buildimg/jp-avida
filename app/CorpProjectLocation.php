<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CorpProjectLocation extends Model
{
    protected $connection = 'mysql_corp';
    protected $table = 'project_location';
    public $timestamps = false;
}
