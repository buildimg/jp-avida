<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Avida Land | Avida Homes, House &amp; Lots and Condos</title>
        <link rel="icon" type="image/png" href="{{ asset('images/favicon.ico') }}">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <link href="{{ asset('css/frontend/app.css') }}?v=2" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        @yield('styles')
    </head>
    <body class="body-pad-top">

    <nav class="navbar-main navbar navbar-default navbar-fixed-top t-ease-fast" role="navigation">

        <!-- ====================== TOP BAR - BLACK ====================== -->
        <div class="nav-thin text-white p-y-1 hidden-xs">
            <div class="container-fluid">
                <div class="cleafix">
                    <div class="nav-sites pull-left">
                        <ul class="list-inline pull-left m-b-0 t-ease">
                            <li>
                                <a class="other-site c-pointer text-white">
                                    <i class="fa fa-chevron-circle-right fa-lg m-r-2 t-ease-fast" aria-hidden="true"></i>Other Ayala Land Sites
                                </a>
                            </li>
                            <li class="other-site-list" style="display: none;">
                                <a class="t-ease" href="http://www.atayala.com/?_ga=1.64700071.945108031.1486002555">Ayala Land</a>
                                <a class="t-ease" href="http://www.ayalalandpremier.com/">Ayala Land Premier</a>
                                <a class="t-ease" href="http://www.alveoland.com.ph/">Alveo</a>
                                <a class="t-ease" href="http://www.avidaland.com/">Avida</a>
                                <a class="t-ease" href="http://www.amaialand.com/">Amaia</a>
                                <a class="t-ease" href="http://www.bellavita.ph/">BellaVita</a>
                            </li>
                        </ul>
                        <select onChange="window.location.href=this.value" style="display: none;">
                            <option value="Other Ayala Land Sites">Other Ayala Land Sites</option>
                            <option value="http://www.atayala.com/?_ga=1.64700071.945108031.1486002555">Ayala Land</option>
                            <option value="http://www.ayalalandpremier.com/">Ayala Land Premier</option>
                            <option value="http://www.alveoland.com.ph/">Alveo</option>
                            <option value="http://www.avidaland.com/">Avida</option>
                            <option value="http://www.amaialand.com/">Amaia</option>
                            <option value="http://www.bellavita.ph/">BellaVita</option>
                        </select>
                    </div>
                    <div class="select-language pull-right">
                        <select onchange="openWindow(this)">
                            <option value="http://jp.avidaland.com.ph/">Japanese (JP)</option>
                            <option value="http://avidaland.com/">English (US)</option>
                        </select>
                    </div>
                    <ul class="list-inline pull-right m-b-0 social-media">
                        <li><a class="fb" href="https://www.facebook.com/AvidaLandPH/" target="_blank"></a></li>
                        <li><a class="tw" href="https://twitter.com/avidalandph" target="_blank"></a></li>
                        <li><a class="ig" href="https://www.instagram.com/avidalandph/" target="_blank"></a></li>
                        <li><a class="yt" href="https://www.youtube.com/user/avidaofficial" target="_blank"></a></li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- ====================== NAV HEADER ====================== -->
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle showSidebar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ route('home') }}">
                    <img src="{{ asset('images/avida-land-logo.png') }}" alt="Avida logo">
                </a>
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <!-- SEARCH THE WEBSITE -->
                {{-- <form class="navbar-form navbar-right" method="GET" action="search-results.php">
                    <button class="btn btn-default navbar-btn-search-close" style="display: none;" type="button"><i class="fa fa-remove"></i></button>
                    <div class="form-group">
                        <input type="hidden" name="page" value="1">
                        <input type="text" class="form-control" name="query" placeholder="Search the website">
                    </div>
                    <button class="btn btn-default"><i class="fa fa-search"></i></button>
                </form> --}}
                <button type="button" class="btn btn-default navbar-btn navbar-btn-search pull-right" style="display: none;"><i class="fa fa-search"></i></button>
                <!-- NAV BAR -->
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle navProperties" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">物件のご紹介 <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a class="p-y-2 clearfix" href="{{ route('houseandlot') }}">House and Lot</a></li>
                        <li><a class="p-y-2 clearfix" href="{{ route('condominium') }}">コンドミニアム</a></li>
                        <li><a class="p-y-2 clearfix" href="{{ route('office') }}">オフィス</a></li>
                      </ul>
                    </li>
                    <li><a href="http://leasing.avidaland.com/" target="_blank">Leasing</a></li>
                    <li><a href="{{ route('brandhistory') }}">会社紹介</a></li>
                    <li><a href="{{ route('contactus') }}">連絡先</a></li>
                    <li><a href="http://www.pursuitofpassion.ph" target="_blank" class="pop-logo"><img src="{{ asset('images/pop.png') }}" alt="Pursuit of Passsion logo"></a></li>
                </ul>
            </div>
            <div class="navbar-icon" style="display: none;">
                <ul class="list-inline m-b-0">
                    <li class="navbar-icon-search"><a class="show-modal-contact-basic" href="javascript:void(0);"><i class="fa fa-phone text-red"></i></a></li>
                    <li class="navbar-icon-search"><a class="c-pointer show-modal-search-basic"><i class="fa fa-search text-red"></i></a></li>
                </ul>
            </div>
        </div>
    {{--  <!-- ====================== SEARCH BAR ====================== -->
    <div class="search-bar">
        <!-- SEARCH BAR CONTAINER -->
        <div class="search-bar-container center-block">
            <!-- FORM -->
            <form method="GET" action="#">
                <div class="search-option">
                    <div class="row row-gap">
                        <!-- LOCATION DROPDOWN -->
                        <div class="col-xs-12 col-sm-4 col-gap">
                            <div class="c-input-select">
                                <select class="form-control sb-location" id="sb-location" name="city">
                                    <option value="" class="geo-location-default">Choose a Location Below</option>
                                </select>
                            </div>
                        </div>
                        <!-- PROJECT TYPE -->
                        <div class="col-xs-12 col-sm-4 col-gap hidden-xs">
                            <div class="c-input-select">
                                <select class="form-control sb-type" name="type" id="sb-type">
                                    <option value="">Any Type</option>
                                </select>
                            </div>
                        </div>
                        <!-- PRICE RANGE -->
                        <div class="col-xs-12 col-sm-4 col-gap hidden-xs">
                            <div class="c-input-select">
                                <select class="form-control sb-price" name="price" id="sb-price">
                                    <option value="">Any Price</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- SEARCH ACTION -->
                <div class="search-action">
                    <div class="clearfix text-center">
                        <button class="btn btn-default btn-search hidden-xs"><i class="fa fa-lg fa-search"></i></button>
                        <button type="button" class="btn btn-default btn-search-advanced showModalSearch"><i class="fa fa-lg fa-gear"></i></button>
                    </div>
                </div>
            </form> <!-- // FORM -->
        </div>
        <span class="close-sb" style="display: none;">&times;</span>
    </div>  --}}
    </nav>

    <div class="sidebar-nav">
        <div class="mask-modal showSidebar"></div>
        <div class="sidebar-nav-content">
            <div class="panel panel-default" style="border: 0;">
                <div class="panel-heading">
                    <div class="clearfix">
                        <div class="pull-left">
                            <a href="#" class="showSidebar text-muted"><i class="fa fa-remove fa-lg m-t-2"></i></a>
                        </div>
                        <div class="pull-right">
                            <select onchange="openWindow(this)" class="form-control input-sm">
                                <option value="http://avidaland.com/">English (US)</option>
                                <option value="http://jp.avidaland.com.ph/">Japanese (JP)</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <ul class="list-unstyled">
                        <li><a href="{{ route('home') }}">ホームページ</a></li>
                        <li><a href="{{ route('brandhistory') }}">会社紹介</a></li>
                        <li><a href="{{ route('properties') }}">物件のご紹介</a></li>
                        <li><a href="http://leasing.avidaland.com/" target="_blank">Leasing</a></li>
                        <li><a href="http://www.pursuitofpassion.ph" target="_blank"><img src="{{ asset('images/pop.png') }}" alt="Pursuit of Passsion logo"></a></li>
                        <li><a href="{{ route('contactus') }}">連絡先</a></li>
                        <li><a href="{{ route('privacypolicy') }}">個人情報保護に関する方針</a></li>
                        <li><a href="{{ route('termsandconditions') }}">条件</a></li>
                    </ul>
                </div>
            </div>
            <div class="p-x-3">
                <div class="form-group">
                    <div class="c-input-select c-input-select-normal">
                        <select class="form-control" onChange="window.location.href=this.value">
                            <option value="Other Ayala Land Sites">Other Ayala Land Sites</option>
                            <option value="http://www.atayala.com/?_ga=1.64700071.945108031.1486002555">Ayala Land</option>
                            <option value="http://www.ayalalandpremier.com/">Ayala Land Premier</option>
                            <option value="http://www.alveoland.com.ph/">Alveo</option>
                            <option value="http://www.avidaland.com/">Avida</option>
                            <option value="http://www.amaialand.com/">Amaia</option>
                            <option value="http://www.bellavita.ph/">BellaVita</option>
                        </select>
                    </div>
                </div>
                {{-- <form method="GET" action="">
                    <div class="form-group">
                        <input type="hidden" name="page" value="1">
                        <input type="text" class="form-control" name="query" placeholder="Search the website">
                    </div>
                </form> --}}
                <div class="form-group">
                    <ul class="list-inline social-media-icons p-y-2 text-center">
                        <li><a class="fb" href="https://www.facebook.com/AvidaLandPH/" target="_blank"></a></li>
                        <li><a class="tw" href="https://twitter.com/avidalandph" target="_blank"></a></li>
                        <li><a class="ig" href="https://www.instagram.com/avidalandph/" target="_blank"></a></li>
                        <li><a class="yt" href="https://www.youtube.com/user/avidaofficial" target="_blank"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>