<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JPProjectNeighborhood extends Model
{
    protected $table = 'jp_project_neighborhoods';
}
