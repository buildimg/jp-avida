		<!-- FOOTER -->
		<footer class="footer text-center">
		    <div class="container">
                <ul class="list-inline list-useful-link m-b-6">
                    <li><a class="p-a-3" href="http://avidaland.com">ホームページ</a></li>
                    <li><a class="p-a-3" href="{{ route('brandhistory') }}">会社紹介</a></li>
                    <li><a class="p-a-3" href="{{ route('properties') }}">物件のご紹介</a></li>
                    <li><a class="p-a-3" href="http://leasing.avidaland.com/?_ga=2.198747439.1607037439.1518570624-781602320.1518570624" target="_blank">Leasing</a></li>
                    <li><a class="p-a-3" href="http://www.pursuitofpassion.ph" target="_blank"><img src="{{ asset('images/pursuit-of-passsion-logo-white.png') }}" alt="Pursuit of Passsion logo"></a></li>
                    <li><a class="p-a-3" href="{{ route('contactus') }}">連絡先</a></li>
                    <li><a class="p-a-3" href="{{ route('privacypolicy') }}">個人情報保護に関する方針</a></li>
                    <li><a class="p-a-3" href="{{ route('termsandconditions') }}">条件</a></li>
                </ul>
				<hr>
                <h3 class="m-t-6">Get in touch with us</h3>
                <ul class="list-unstyled">
                    <li class="p-b-1"><small class="text-gray">If you are in the U.S.A, you can call us via our toll free number below:</small></li>
                    <li><a href="Tel:18555726632" class="text-white">1-855-572-6632</a></li>
                </ul>
                <ul class="list-unstyled">
                    <li class="p-b-1"><small class="text-gray">If you are in the Philippines, you may get in touch with us through the below contact numbers:</small></li>
                    <li>Sales Hotline - <a href="tel:+6328485200" class="text-white">(+632) 8485200</a> or <a href="tel:+639178828432" class="text-white">(+63917) 8828432</a></li>
                    <li>Head Office Trunk line - <a href="tel:+6329882111" class="text-white">(+632) 9882111</a></li>
                </ul>

                <ul class="list-inline social-media-icons m-t-6">
                    <li><a class="fb" href="https://www.facebook.com/AvidaLandPH/" target="_blank"></a></li>
                    <li><a class="tw" href="https://twitter.com/avidalandph" target="_blank"></a></li>
                    <li><a class="ig" href="https://www.instagram.com/avidalandph/" target="_blank"></a></li>
                    <li><a class="yt" href="https://www.youtube.com/user/avidaofficial" target="_blank"></a></li>
                </ul>
		    </div>
		</footer>

        <script src="{{ asset('js/frontend/app.js') }}"></script>
        <script>
            function openWindow(select) {
                var value = select.options[select.selectedIndex].value;
                window.open(value, 'newwindow')
            }
        </script>

        <!-- begin SnapEngage code -->

        <script type="text/javascript">

          (function() {

            var se = document.createElement('script'); se.type = 'text/javascript'; se.async = true;

            se.src = '//storage.googleapis.com/code.snapengage.com/js/79fd73b0-6ae3-4f17-a71b-4c063461ce4b.js';

            var done = false;

            se.onload = se.onreadystatechange = function() {

              if (!done&&(!this.readyState||this.readyState==='loaded'||this.readyState==='complete')) {

                done = true;
                
                SnapABug.setCallback('Close', function(type, status) {
                    dataLayer.push({'event':'snapengage-close'});
                });
                SnapABug.setCallback('ChatMessageReceived', function(agent, msg) {
        dataLayer.push({'event':'snapengage-chat-message-received'});
                });
                SnapABug.setCallback('ChatMessageSent', function(msg) {
        dataLayer.push({'event':'snapengage-chat-message-sent'});
                });
                SnapABug.setCallback('MessageSubmit', function(email, msg) {
        dataLayer.push({'event':'snapengage-message-submit'});
                });
                SnapABug.setCallback('Open', function(status) {
                    dataLayer.push({'event':'snapengage-open'});
                });
                SnapABug.setCallback('OpenProactive', function(agent, msg) {
        dataLayer.push({'event':'snapengage-open-proactive'});
                });
                SnapABug.setCallback('StartChat', function(email, msg, type) {
        dataLayer.push({'event':'snapengage-start-chat'});
                });
                SnapABug.setCallback('StartCallme', function(phone) {
        dataLayer.push({'event':'snapengage-start-call-me'});
                });

              }

            };

            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(se, s);

          })();

        </script>

        <!-- end SnapEngage code -->
        @yield('scripts')
    </body>
</html>