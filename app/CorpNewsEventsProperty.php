<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CorpNewsEventsProperty extends Model
{
    protected $connection = 'mysql_corp';
    protected $table = 'articles_properties';
    public $timestamps = false;

    public function articles ()
    {
        return $this->hasMany(CorpNewsEvents::class, 'article_ID', 'article_ID');
    }
}
