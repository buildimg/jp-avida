<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CorpProjectAmenity extends Model
{
    protected $connection = 'mysql_corp';
    protected $table = 'project_amenities';
    public $timestamps = false;
    public function amenity_gallery ()
    {
        return $this->hasMany(CorpProjectAmenityGallery::class, 'amenities_ID', 'amenities_ID');
    }
}
