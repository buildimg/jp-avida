<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CorpProject extends Model
{
    protected $connection = 'mysql_corp';
    protected $table = 'projects';
    public $timestamps = false;

    const PROJECT_TYPE = [
        'Condominium'   => 'bg-green',
        'House-Lot'     => 'bg-blue',
        'Office'        => 'bg-yellow'
    ];

    const PROJECT_TYPE_JAPANESE = [
        'Condominium' 	=> 'コンドミニアム',
        'House-Lot' 	=> 'House &#38; Lot',
        'Office' 		=> 'オフィス'
    ];

    const PROJECT_STATUS = [
        'RFO' 			=> 'bg-orange',
        'Pre-Selling' 	=> 'bg-red',
        'Sold-Out' 		=> 'bg-black'
    ];
    
    public function amenity ()
    {
        return $this->hasOne(CorpProjectAmenity::class, 'project_ID', 'project_ID');
    }
    
    public function concept ()
    {
        return $this->hasOne(CorpProjectConcept::class, 'project_ID', 'project_ID');
    }

    public function fp_unit ()
    {
        return $this->hasOne(CorpProjectFloorUnit::class, 'project_ID', 'project_ID');
    }
    
    public function fp_overall ()
    {
        return $this->hasOne(CorpProjectFloorPlanOverall::class, 'project_ID', 'project_ID');
    }

    public function location ()
    {
        return $this->hasOne(CorpProjectLocation::class, 'project_ID', 'project_ID');
    }

    public function constructions ()
    {
        return $this->hasOne(CorpProjectConstruction::class, 'project_ID', 'project_ID');
    }
    
    public function neighborhoods ()
    {
        return $this->hasOne(CorpProjectNeighborhood::class, 'nh_ID', 'project_neighborhood');
    }
}
