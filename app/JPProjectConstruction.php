<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JPProjectConstruction extends Model
{
    protected $table = 'jp_project_constructions';
    public function construction_gallery ()
    {
        return $this->hasMany(JPProjectConstructionGallery::class, 'corp_const_id', 'corp_const_id');
    }
}
