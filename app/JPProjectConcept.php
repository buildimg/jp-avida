<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JPProjectConcept extends Model
{
    protected $table = 'jp_project_concepts';
    public function concept_gallery ()
    {
        return $this->hasMany(CorpProjectConceptGallery::class, 'concept_ID', 'concept_ID');
    }
}
