<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJPProjectConceptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jp_project_concepts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('concept_tagline', 100)->nullable();
            $table->text('concept_text')->nullable();
            $table->integer('corp_concept_id')->nullable();
            $table->integer('corp_project_id')->nullable();
            $table->tinyInteger('status')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jp_project_concepts');
    }
}
