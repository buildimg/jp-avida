<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Hash;
use Auth;
use \App\CorpProject;

use \App\JPProject;

class ProjectController extends Controller
{
    public function projects (Request $request)
    {	
		$Projects = CorpProject::with(['amenity', 'concept', 'fp_unit', 'fp_unit.unit_images', 'fp_overall' , 'fp_overall.fp_overall_images', 'location']);
		
		$JPProject = JPProject::where('status', 1)
		->select(['corp_project_id', 'featured'])
		->get(); 
        if(request()->ajax()){
			$Projects = $Projects->where(function ($query) use($request) {
				$query->where('project_name', 'like', '%' . $request->search_data . '%');
			})
			->where('project_type', "!=", "Estate")
			->paginate(10);
            return Response()->json(view('cms.projects.paginate', ['Projects' => $Projects, 'JPProject' => $JPProject])->render());
		}
		$Projects = $Projects
		->where('project_type', "!=", "Estate")
		->paginate(10);

        return view('cms.projects.projects', ['Projects' => $Projects, 'JPProject' => $JPProject]);
    }

    public function mark_project (Request $request) 
    {
    	$JPProject = JPProject::where('corp_project_id', $request->id)->first(); 
    	
    	if ($JPProject)
    	{
    		if ($JPProject->status == 0)
    		{
	    		$JPProject->status = 1;
	    		$JPProject->save();
    			return response()->json(['res_code' => 1, 'res_msg' => 'Project successfully marked']);
    		}

    		$JPProject->status = 0;
    		$JPProject->save();

    		return response()->json(['res_code' => 1, 'res_msg' => 'Project successfully unmarked']);
    	}
    	else
    	{
			
			$CorpProject = CorpProject::with(['amenity',
				'amenity.amenity_gallery',
				'concept',
				'concept.concept_gallery',
				'fp_overall',
				'fp_overall.fp_overall_images',
				'fp_unit',
				'fp_unit.unit_images',
				'location',
				'constructions',
				'constructions.construction_galleries',
				'neighborhoods',
				'neighborhoods.neighborhood_attractions'
			 ])
			 ->where('project_ID', $request->id)
			 ->first();   
			
    		if ($CorpProject)
    		{
    			$JPProject 					= new JPProject();
				$JPProject->corp_project_id = $CorpProject->project_ID;
	    		$JPProject->status 			= 1;
	    		$JPProject->save();	

				if ($CorpProject->amenity)
				{

					$JPProjectAmenity 					= new \App\JPProjectAmenity();
					$JPProjectAmenity->content 			= $CorpProject->amenity->amenities_overall_text;
					$JPProjectAmenity->corp_amenity_id 	= $CorpProject->amenity->amenities_ID;
					$JPProjectAmenity->corp_project_id 	= $CorpProject->amenity->project_ID;
					$JPProjectAmenity->save();

					if ($CorpProject->amenity->amenity_gallery)
					{
						foreach ($CorpProject->amenity->amenity_gallery as $amenity_gallery) 
						{
							$JPProjectAmenityGallery 					= new \App\JPProjectAmenityGallery();
							$JPProjectAmenityGallery->gallery_title 	= $amenity_gallery->gallery_title;
							$JPProjectAmenityGallery->corp_gallery_id 	= $amenity_gallery->gallery_ID;
							$JPProjectAmenityGallery->corp_amenity_id 	= $amenity_gallery->amenities_ID;
							$JPProjectAmenityGallery->save();
						}
					}
				}
				
				if ($CorpProject->concept)
				{
					$JPProjectConcept 					= new \App\JPProjectConcept();
					$JPProjectConcept->concept_tagline 	= $CorpProject->concept->concept_tagline;
					$JPProjectConcept->concept_text 	= $CorpProject->concept->concept_text;
					$JPProjectConcept->corp_concept_id 	= $CorpProject->concept->concept_ID;
					$JPProjectConcept->corp_project_id 	= $CorpProject->concept->project_ID;
					$JPProjectConcept->save();
				}

				if ($CorpProject->fp_overall)
				{
					$JPProjectFloorPlan 					= new \App\JPProjectFloorPlan();
					$JPProjectFloorPlan->floor_plan_text 	= $CorpProject->fp_overall->FPoverall_overall_text;
					$JPProjectFloorPlan->corp_floor_plan_id = $CorpProject->fp_overall->FPoverall_ID;
					$JPProjectFloorPlan->corp_project_id 	= $CorpProject->fp_overall->project_ID;
					$JPProjectFloorPlan->save();

					if ($CorpProject->fp_overall->fp_overall_images)
					{
						foreach ($CorpProject->fp_overall->fp_overall_images as $fpo_images) 
						{
							$JPProjectFloorPlanGallery 						= new \App\JPProjectFloorPlanGallery();
							$JPProjectFloorPlanGallery->title 				= $fpo_images->overall_title;
							$JPProjectFloorPlanGallery->subtitle 			= $fpo_images->overall_subtitle;
							$JPProjectFloorPlanGallery->content 			= $fpo_images->overall_text;
							$JPProjectFloorPlanGallery->corp_overall_id 	= $fpo_images->overall_ID;
							$JPProjectFloorPlanGallery->corp_fpoverall_id 	= $fpo_images->FPoverall_ID;
							$JPProjectFloorPlanGallery->save();
						}
					}
				}
				
				if ($CorpProject->fp_unit)
				{
					$JPProjectFloorPlanUnit 					= new \App\JPProjectFloorPlanUnit();
					$JPProjectFloorPlanUnit->content 			= $CorpProject->fp_unit->FPunits_overall_text;
					$JPProjectFloorPlanUnit->corp_fp_unit_id 	= $CorpProject->fp_unit->FPunits_ID;
					$JPProjectFloorPlanUnit->corp_project_id 	= $CorpProject->fp_unit->project_ID;
					$JPProjectFloorPlanUnit->save();

					foreach ($CorpProject->fp_unit->unit_images as $uni_images)
					{
						$JPProjectFloorPlanUnitGallery 					= new \App\JPProjectFloorPlanUnitGallery();
						$JPProjectFloorPlanUnitGallery->title 			= $uni_images->units_title;
						$JPProjectFloorPlanUnitGallery->subtitle 		= $uni_images->units_subtitle;
						$JPProjectFloorPlanUnitGallery->corp_unit_id 	= $uni_images->units_ID;
						$JPProjectFloorPlanUnitGallery->corp_fp_unit_id = $uni_images->FPunits_ID;
						$JPProjectFloorPlanUnitGallery->save();
					}
				}

				if ($CorpProject->location)
				{
					$JPProjectLocation 						= new \App\JPProjectLocation();
					$JPProjectLocation->content 			= $CorpProject->location->location_text;
					$JPProjectLocation->corp_location_id 	= $CorpProject->location->location_ID;
					$JPProjectLocation->corp_project_id 	= $CorpProject->location->project_ID;
					$JPProjectLocation->save();
				}
				
				if ($CorpProject->constructions)
				{
					$JPProjectConstruction 					= new \App\JPProjectConstruction();
					$JPProjectConstruction->content 		= $CorpProject->constructions->const_overall_text;
					$JPProjectConstruction->corp_const_id 	= $CorpProject->constructions->const_ID;
					$JPProjectConstruction->corp_project_id = $CorpProject->project_ID;
					$JPProjectConstruction->save();

					if ($CorpProject->constructions->construction_galleries)
					{
						foreach ($CorpProject->constructions->construction_galleries as $construction_images) 
						{
							$JPProjectConstructionGallery 						= new \App\JPProjectConstructionGallery();
							$JPProjectConstructionGallery->image_title 			= $construction_images->const_img_title;
							$JPProjectConstructionGallery->corp_const_img_id 	= $construction_images->const_img_ID;
							$JPProjectConstructionGallery->corp_const_id 		= $construction_images->const_ID;
							$JPProjectConstructionGallery->save();
						}
					}
				}	
				
				if ($CorpProject->neighborhoods)
				{
					
					$JPProjectNeighborhood 					= new \App\JPProjectNeighborhood();
					$JPProjectNeighborhood->corp_nh_id 		= $CorpProject->neighborhoods->nh_ID;
					$JPProjectNeighborhood->save();

					if ($CorpProject->neighborhoods->neighborhood_attractions)
					{
						foreach ($CorpProject->neighborhoods->neighborhood_attractions as $attraction)
						{
							$JPProjectNeighborhoodAttraction = new \App\JPProjectNeighborhoodAttraction();
							$JPProjectNeighborhoodAttraction->content = $attraction->att_text;
							$JPProjectNeighborhoodAttraction->corp_att_id = $attraction->att_ID;
							$JPProjectNeighborhoodAttraction->corp_nh_id = $attraction->nh_ID;
							$JPProjectNeighborhoodAttraction->save();
						}
					}
				}
    			return response()->json(['res_code' => 1, 'res_msg' => 'Project successfully marked']);
    		}
    	}

    }

    public function toggle_featured_project (Request $request) 
    {
    	$JPProject = JPProject::where('corp_project_id', $request->id)->first(); 
    	if ($JPProject)
    	{
    		if ($JPProject->featured == 0)
    		{
	    		$JPProject->featured = 1;
	    		$JPProject->save();
    	return json_encode($JPProject);
    			return response()->json(['res_code' => 0, 'res_msg' => 'Project successfully marked as featured']);
    		}

    		$JPProject->featured = 0;
    		$JPProject->save();

    		return response()->json(['res_code' => 0, 'res_msg' => 'Project successfully unmarked as featured']);
    	}
    	else
    	{	
    		return response()->json(['res_code' => 1, 'res_msg' => 'Project is not yet marked']);
    	}
	}
	
	public function modal_data (Request $request) 
	{
		$tab_id = $request->tab_id;
		$JPProject = JPProject::with([
			'concept',
			'concept.concept_gallery',
			'amenity', 
			'amenity.amenity_gallery',
			'fp_unit',
			'fp_unit.unit_images',
			'fp_overall',
			'fp_overall.unit_images',
			'location',
			'constructions',
			'constructions.construction_gallery'
		])->where('corp_project_id', $request->project_id)->first();
		
		$project_folder = '';
		$project_name = '';
		$project_id = '';

		$CorpProject = NULL;
		if($JPProject)
		{
			$CorpProject = CorpProject::with([
				'fp_unit', 
				'fp_unit.unit_images', 
				'fp_overall', 
				'fp_overall.fp_overall_images',
				'constructions',
				'constructions.construction_galleries',
			])->where('project_ID', $JPProject->corp_project_id)->first();
			if ($CorpProject)
			{
				$project_folder = $CorpProject->project_upload_loc;
				$project_name 	= $CorpProject->project_name;
			}
		}




		$CorpProjectAmenityGallery = NULL;
		if ($JPProject->amenity)
		{
			$CorpProjectAmenityGallery = \App\CorpProjectAmenityGallery::where('amenities_ID', $JPProject->amenity->corp_amenity_id)->get();
		}

		$CorpProjectConceptGallery = NULL;
		if ($JPProject->concept)
		{
			$CorpProjectConceptGallery = \App\CorpProjectConceptGallery::where('concept_ID', $JPProject->concept->corp_concept_id)->get();
		}
		
		// $CorpProjectConceptGallery = NULL;
		// if ($JPProject->fp_unit)
		// {
		// 	$CorpProjectConceptGallery = \App\CorpProjectConceptGallery::where('concept_ID', $JPProject->concept->corp_concept_id)->get();
		// }



		$tabs = [
			[
				'tab' 	=> 5,
				'icon' => 'fa-pencil',
				'text' => 'Project Concept'
			],
			[
				'tab' 	=> 1,
				'icon' => 'fa-map-marker',
				'text' => 'Location'
			],
			[
				'tab' 	=> 2,
				'icon' => 'fa-picture-o',
				'text' => 'Overall Floor Plan'
			],
			[
				'tab' 	=> 3,
				'icon' => 'fa-picture-o',
				'text' => 'Floor Plan Unit'
			],
			[
				'tab' 	=> 4,
				'icon' => 'fa-picture-o',
				'text' => 'Amenities & Gallery'
			],
			[
				'tab' 	=> 6,
				'icon' => 'fa-picture-o',
				'text' => 'Constructions'
			],
		];

		return view('cms.projects.modal_data', compact('tab_id', 'JPProject', 'tabs', 'CorpProjectAmenityGallery', 'CorpProjectConceptGallery', 'CorpProject', 'project_folder', 'project_name'))->render();
	}

	public function save_project_details (Request $request)
	{
		$JPProject = \App\JPProject::where('id', $request->project_id)->first();
		if (!$JPProject)
		{

		}
		$JPProject->project_name = $request->project_name;
		$JPProject->save();

		$JPProjectLocation = \App\JPProjectLocation::where('corp_project_id', $JPProject->corp_project_id)->first();
		$JPProjectFloorPlanUnit = \App\JPProjectFloorPlanUnit::where('corp_project_id', $JPProject->corp_project_id)->first();
		$JPProjectAmenity = \App\JPProjectAmenity::where('corp_project_id', $JPProject->corp_project_id)->first();
		$JPProjectConcept = \App\JPProjectConcept::where('corp_project_id', $JPProject->corp_project_id)->first();

		if ($JPProjectLocation)
		{
			$JPProjectLocation->content = $request->location;
			$JPProjectLocation->save();
		}

		if ($JPProjectFloorPlanUnit)
		{
			$JPProjectFloorPlanUnit->content = $request->unit_details;
			$JPProjectFloorPlanUnit->save();
		}
		
		if ($JPProjectAmenity)
		{
			$JPProjectAmenity->content = $request->amenity;
			$JPProjectAmenity->save();
		}

		if ($JPProjectConcept)
		{
			$JPProjectConcept->concept_tagline = $request->concept_tagline;
			$JPProjectConcept->concept_text = $request->concept_content;
			$JPProjectConcept->save(); 
		}

		if ($request->amenity_gallery_id)
		{
			foreach ($request->amenity_gallery_id as $key => $gallery_id)
			{
				$JPProjectAmenityGallery = \App\JPProjectAmenityGallery::where('id', $gallery_id)->first();
				
				if ($JPProjectAmenityGallery)
				{
					$JPProjectAmenityGallery->gallery_title = $request->amenity_gallery_text[$key];
					$JPProjectAmenityGallery->save();
				}
			}
		}

		if ($request->floor_plan_unit_gallery_id)
		{
			foreach ($request->floor_plan_unit_gallery_id as $key => $floor_plan_unit_id)
			{
				$JPProjectFloorPlanUnitGallery = \App\JPProjectFloorPlanUnitGallery::where('id', $floor_plan_unit_id)->first();
				if ($JPProjectFloorPlanUnitGallery)
				{
					$JPProjectFloorPlanUnitGallery->title 	= $request->floor_plan_unit_gallery_text[$key];
					$JPProjectFloorPlanUnitGallery->save();
				}
			}
		}

		$JPProjectFloorPlan = \App\JPProjectFloorPlan::where('id', $request->overall_floor_plan_id)->first();
		$JPProjectFloorPlan->floor_plan_text = $request->overall_floor_plan_details;
		$JPProjectFloorPlan->save();

		if ($request->overall_floor_plan_gallery_id)
		{	
			foreach ($request->overall_floor_plan_gallery_id as $key => $overall_floor_plan_gallery_id)
			{
				$JPProjectFloorPlanGallery = \App\JPProjectFloorPlanGallery::where('id', $overall_floor_plan_gallery_id)->first();
				if ($JPProjectFloorPlanGallery)
				{
					$JPProjectFloorPlanGallery->title 	= $request->overall_floor_plan_gallery_text[$key];
					$JPProjectFloorPlanGallery->save();
				}
			}
		}
			
		
		$JPProjectConstruction = \App\JPProjectConstruction::where('id', $request->construction_id)->first();
		$JPProjectConstruction->content = $request->construction;
		$JPProjectConstruction->save();
		
		if ($request->construction_gallery_id)
		{
			foreach ($request->construction_gallery_id as $key => $construction_gallery_id)
			{
				$JPProjectConstructionGallery = \App\JPProjectConstructionGallery::where('id', $construction_gallery_id)->first();
				if ($JPProjectConstructionGallery)
				{
					$JPProjectConstructionGallery->image_title 	= $request->construction_gallery_text[$key];
					$JPProjectConstructionGallery->save();
				}
			}
		}
			
		return json_encode($JPProjectConcept);
	}

	
    public function change_profile_modal() 
    {
        return view('cms.user_details.partials.modal_data')->render();
    }

    public function save_profile_data (Request $request) 
    {
		$User = \App\User::where('id', Auth::user()->id)->first();
		
		$validator = \Validator::make($request->all(), 
		[
			'input_name' => 'nullable',
			'input_input_old_password' => 'nullable',
			'input_new_password' => 'confirmed'
		]);

		if ($validator->fails())
		{
			return response()->json(['res_code' => 2, 'res_msg' => 'The password and confirmation does not matched.', 'error_msg' => $validator->getMessageBag()]);
			return json_encode($validator->getMessageBag());
		}

		if ($User) 
		{
			$User->name = $request->input_name;
			if ($request->input_old_password && $request->input_new_password)
			{
				if (\Hash::check($request->input_old_password, Auth::user()->password)) 
				{
					$validator = \Validator::make($request->all(), 
					[
						'input_new_password' => 'required|confirmed'
					]);

					if ($validator->fails())
					{
						return response()->json(['res_code' => 2, 'res_msg' => 'The password and confirmation does not matched.', 'error_msg' => $validator->getMessageBag()]);
					}
					$User->password = bcrypt($request->input_new_password);
					$User->save();
					return response()->json(['res_code' => 0, 'res_msg' => 'Information successfully updated.']);
				}	
				
				return response()->json(['res_code' => 2, 'res_msg' => 'Old password is incorrect.']);
			}
			$User->save();
			return response()->json(['res_code' => 0, 'res_msg' => 'Information successfully updated.']);
		}

    }
}