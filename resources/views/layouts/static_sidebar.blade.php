<div class="nav-static">
	<ul class="nav list-unstyled">
	    <li><a class="{{ Request::is('contact-us') ? 'active' : '' }}" href="{{ route('contactus') }}">連絡先</a></li>
	    <li><a class="{{ Request::is('privacy-policy') ? 'active' : '' }}" href="{{ route('privacypolicy') }}">個人情報保護に関する方針</a></li>
	    <li><a class="{{ Request::is('terms-and-conditions') ? 'active' : '' }}" href="{{ route('termsandconditions') }}">条件</a></li>
	</ul>
</div>