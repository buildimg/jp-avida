<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JPProjectAmenity extends Model
{
    protected $table = 'jp_project_amenities';
    public function amenity_gallery ()
    {
        return $this->hasMany(JPProjectAmenityGallery::class, 'corp_amenity_id', 'corp_amenity_id');
    }
}
