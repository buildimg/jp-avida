try {
    window.$ = window.jQuery = require('jquery');

    require('bootstrap-sass');
} catch (e) {}
        
$('.showSidebar').click(function(){
  $('body').toggleClass('navbar-sm-toggle');
});

$('.clickMobPropType').click(function(){
    $('#collapseMobPropType').collapse('toggle');
    $(this).find('i').toggleClass('fa-rotate-270')
});


$('.clickMobContact').click(function(){
    $('#collapseMobContact').collapse('toggle');
    $(this).find('i').toggleClass('fa-rotate-270')
});

$('.other-site').click(function(){
    $('.other-site-list').fadeToggle(250);
    $(this).find('i').toggleClass('fa-rotate-180')
});