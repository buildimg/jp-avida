<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JPProjectFloorPlan extends Model
{
    protected $table = 'jp_project_floor_plans';
    public function unit_images ()
    {
        return $this->hasMany(JPProjectFloorPlanGallery::class, 'corp_fpoverall_id', 'corp_floor_plan_id');
    }
}
