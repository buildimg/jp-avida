<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CorpProjectNeighborhoodAttraction extends Model
{
    protected $connection = 'mysql_corp';
    protected $table = 'neighborhoods_attractions';
    public $timestamps = false;
}
