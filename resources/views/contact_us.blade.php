@extends('layouts.main')
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<h1>Contact Us</h1>
				<hr>

				<h4><strong>本社</strong></h4>
				<p>
					アビダランド社 <br>
					（タギッグ市 1634 ボニファシオ・グローバル・シティボニファシオ・トライアングル40th ストリート・ノース#909） <br>
					(+ 63 2) 988-2111
				</p>
				<br>
				<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3861.675478763417!2d121.053104!3d14.5605413!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397c90853e55555%3A0x16d598ecc38d6fde!2sAvida+Land+Corp!5e0!3m2!1sen!2sph!4v1519714335169" width="100%" height="360" frameborder="0" style="border:0" allowfullscreen></iframe>
				<br>
				
			</div>
			<div class="col-md-3">
				@include('layouts.static_sidebar')
			</div>
		</div>
		<br>
		<br>
	</div>
@endsection