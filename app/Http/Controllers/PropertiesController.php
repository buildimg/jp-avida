<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\CorpProject;

class PropertiesController extends Controller
{
    public function properties()
    {
        $selected_jp_projects = [];
		$JPProject = \App\JPProject::where('status', 1)->select(['corp_project_id', 'project_name'])->get();
		foreach ($JPProject as $proj)
		{
			$selected_jp_projects[] = $proj->corp_project_id;
        }
        $CorpProject = CorpProject::whereIn('project_ID', $selected_jp_projects)->where('project_type', '!=', 'Estate')->get();
        return view('properties', compact('CorpProject', 'JPProject'));
    }
    public function houseandlot()
    {
        $selected_jp_projects = [];
		$JPProject = \App\JPProject::where('status', 1)->select(['corp_project_id', 'project_name'])->get();
		foreach ($JPProject as $proj)
		{
			$selected_jp_projects[] = $proj->corp_project_id;
        }
    	$CorpProject = CorpProject::whereIn('project_ID', $selected_jp_projects)->where('project_type', 'House-lot')->get();
        return view('properties_house_and_lot', compact('CorpProject', 'JPProject'));
    }
    public function condominium()
    {
        $selected_jp_projects = [];
		$JPProject = \App\JPProject::where('status', 1)->select(['corp_project_id', 'project_name'])->get();
		foreach ($JPProject as $proj)
		{
			$selected_jp_projects[] = $proj->corp_project_id;
        }
    	$CorpProject = CorpProject::whereIn('project_ID', $selected_jp_projects)->where('project_type', 'Condominium')->get();
        return view('properties_condo', compact('CorpProject', 'JPProject'));
    }
    public function office()
    {
        $selected_jp_projects = [];
		$JPProject = \App\JPProject::where('status', 1)->select(['corp_project_id', 'project_name'])->get();
		foreach ($JPProject as $proj)
		{
			$selected_jp_projects[] = $proj->corp_project_id;
        }
    	$CorpProject = CorpProject::whereIn('project_ID', $selected_jp_projects)->where('project_type', 'Office')->get();
        return view('properties_office', compact('CorpProject', 'JPProject'));
    }
}
