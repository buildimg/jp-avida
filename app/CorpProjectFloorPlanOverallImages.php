<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CorpProjectFloorPlanOverallImages extends Model
{
    protected $connection = 'mysql_corp';
    protected $table = 'project_fp_overall_img';
    public $timestamps = false;
}
