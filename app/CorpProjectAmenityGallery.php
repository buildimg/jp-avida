<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CorpProjectAmenityGallery extends Model
{
    protected $connection = 'mysql_corp';
    protected $table = 'project_amenities_gallery';
    public $timestamps = false;
}
