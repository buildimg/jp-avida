<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CorpProjectConstruction extends Model
{
    protected $connection = 'mysql_corp';
    protected $table = 'project_construction';
    public $timestamps = false;

    public function construction_galleries ()
    {
        return $this->hasMany(CorpProjectConstructionGallery::class, 'const_ID', 'const_ID');
    }
}
