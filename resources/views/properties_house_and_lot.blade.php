@extends('layouts.main')
@section('styles')
    <style>
        body{
            padding-top: 82px;
        }
        .search-bar {
            display: none;
            position: absolute;
            top: 81px;
            left: 0;
            width: 100%;
        }
    </style>
@endsection
@section('content')
	<div class="container">
		<div class="text-center">
			<h1>House and Lot ({{ $CorpProject->count() }})</h1>
			<hr>
		</div>
        @foreach ($CorpProject as $proj)
            <?php
                $proj_jp = $JPProject->firstWhere('corp_project_id', $proj->project_ID);
            ?>
            <div class="col-xs-6 col-sm-6 col-md-4 col-xs-block">
                <div class="property">
                    <a href="{{ route('project', $proj->project_url_name) }}">
                        <img src="{{ env('AVIDA_URL_PROJECT_UPLOADS') . $proj->project_upload_loc . $proj->project_thumb }}" class="w-100">
                    </a>
                    <h5 class="text-uppercase text-muted small">
                        {{ App\CorpProject::PROJECT_TYPE_JAPANESE[$proj->project_type] }}
                    </h5>
                    <h3 class="property-project-name m-y-1">
                        <a href="{{ route('project', $proj->project_url_name) }}"><strong>{{ $proj_jp ? $proj_jp->project_name ? $proj_jp->project_name : $proj->project_name : $proj->project_name }}</strong></a>
                    </h3>
                    <p class="property-address text-muted m-b-4">
                        @if($proj->project_province == 'Metro Manila') 
                            {{ $proj->project_address.', '.$proj->project_cities }}
                        @else
                            {{ $proj->project_address.', '.$proj->project_cities.', '.$proj->project_province }}
                        @endif
                    </p>
                    <div class="property-details">
                        <div class="row row-gap">
                            <div class="col-xs-6 col-gap">
                                <div class="small">
                                    @if(!empty($proj->project_unit_min) || !empty($proj->project_unit_max))
                                        <span class="text-muted">ユニット面積</span>
                                        <div>{{ $proj->project_unit_min.' - '.$proj->project_unit_max }} sq.m</div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-6 col-gap">
                                <div class="small">
                                    @if(!empty($proj->project_price_min) || !empty($proj->project_price_max))
                                        <span class="text-muted">価格帯</span>
                                        <div>{{ $proj->project_price_min.'M - '.$proj->project_price_max.' M' }}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
	</div>
@endsection