<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJPProjectConstructionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jp_project_constructions', function (Blueprint $table) {
            $table->increments('id');
            $table->text('content')->nullable();
            $table->integer('corp_const_id')->nullable();
            $table->integer('corp_project_id')->nullable();
            $table->tinyInteger('status')->default('1');  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jp_project_constructions');
    }
}
