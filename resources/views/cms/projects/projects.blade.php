@extends('cms.layouts.main')
@section('styles')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-wysiwyg/0.3.3/bootstrap3-wysihtml5.min.css">
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Projects
            {{--  <small>Loading example</small>  --}}
        </h1>
        {{--  <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Pace page</li>
        </ol>  --}}
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
               		<h3 class="box-title">All Projects</h3>
			   <br>
			   <br>
			   <form id="js-search">
					<div class="col-sm-4" style="padding:0">
						<div class="form-group">
							<input type="text" class="form-control" style="border-radius:0" id="input_search" name="input_search">
						</div>
					</div>
					<div class="col-sm-3" style="padding:0">
						<button class="btn btn-flat btn-primary">Search</button>
					</div>
			   </form>
            </div>
			<!-- /.box-header -->
	        @include('cms.projects.paginate')
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>

<!-- /.content-wrapper -->
@endsection

@section('scripts')

	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-wysiwyg/0.3.3/bootstrap3-wysihtml5.all.min.js"></script>
	<script>
		/*$('body').on('click', '.dropdown-actions a', function(){
			var id = $(this).data('id');
			$('a[href="#'+id+'"]').trigger('click');
			$('a[href="#'+id+'"]').parent().addClass('active');
		});*/

	    $(window).on('hashchange', function() {
	        if (window.location.hash) {
	            var page = window.location.hash.replace('#', '');
	            if (page == Number.NaN || page <= 0) {
	                return false;
	            } else {
	                getPosts(page);
	            }
	        }
	    });

	    $(document).ready(function() {
	        $(document).on('click', '.pagination a', function(e) {
	        	loadingData();
	            getPosts($(this).attr('href').split('page=')[1]);
	            e.preventDefault();
	        });
	    });

	    function getPosts(page) {
	        $.ajax({
	            url: '{{ route('cms.projects') }}?page=' + page + '&search_data='+search_data,
	            dataType: 'json',
	        }).done(function(data) {
	            $('.box-data-holder').html(data);
	            location.hash = page;
	        }).fail(function() {
	            alert('Posts could not be loaded.');
	        });
	    }

	    function loadingData() {
	        $('.overlay').toggleClass('hidden');
	    }


	    $('body').on('click', '.js-mark_project_toggle', function (e) {
	    	e.preventDefault();
	    	var id = $(this).data('id');
	    	var currentClickedButton = $(this);
	        loadingData();	
	    	$.ajax({
	    		url 	: "{{ route('cms.mark_project') }}",
	    		type 	: 'POST',
	    		data 	: { id : id, _token : '{{ csrf_token() }}' },
	    		success : function (res) {
	        		loadingData();
	        		if (currentClickedButton.text() == 'Mark')
	        		{
	        			currentClickedButton.text('Unmark');
	        			currentClickedButton.toggleClass('btn-default btn-danger');
	        			currentClickedButton.parent().parent().find('.custom-control-input').attr('disabled', false);
	        			currentClickedButton.parent().parent().find('.dropdown-toggle').attr('disabled', false);
	        		}
	        		else
	        		{
	        			currentClickedButton.text('Mark');	
	        			currentClickedButton.toggleClass('btn-default btn-danger');
	        			currentClickedButton.parent().parent().find('.custom-control-input').attr('disabled', true);
	        			currentClickedButton.parent().parent().find('.dropdown-toggle').attr('disabled', true);
	        		}
	    		}
	    	});
	    });
	    $('body').on('change', '.custom-control-input', function (e) {
	    	var feature = $(this).prop('checked');
	    	var id = $(this).data('id');
	    	$.ajax({
	    		url : "{{ route('cms.toggle_featured_project') }}",
	    		type : 'POST',
	    		data : { id : id, _token : "{{ csrf_token() }}" },
	    		success : function (res) {
	    			console.log(res);
	    		}
	    	});
		})
		
		$('body').on('click', '.dropdown-actions a', function(){
			var project_id 	= $(this).data('project-id');
			var tab_id 		= $(this).data('tab-id');
	        loadingData();	
			$.ajax({
				url : "{{ route('cms.modal_data') }}",
				type : 'POST',
				data : { _token : '{{ csrf_token() }}', project_id : project_id , tab_id : tab_id },
				success : function (res) {
					$('#modal_container').html(res);
					$('#modal-default').modal({ backdrop : 'static' });

		            $('.textarea').wysihtml5({
		                toolbar: {
		                    "font-styles": false, //Font styling, e.g. h1, h2, etc. Default true
		                    "emphasis": true, //Italics, bold, etc. Default true
		                    "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
		                    "html": false, //Button which allows you to edit the generated HTML. Default false
		                    "link": false, //Button to insert a link. Default true
		                    "image": false, //Button to insert an image. Default true,
		                    "color": false, //Button to change color of font  
		                    "blockquote": false, //Blockquote  
		                    "size": 'sm', //default: none, other options are xs, sm, lg
		                    "fa": true
		                }
		            });

		            $('.wysihtml5-toolbar').addClass('list-inline');

	        		loadingData();
				}
			});
		});
		
		$('body').on('hidden.bs.modal', '#modal-default', function () {
			$('#modal_container').html('');
		})

		$('body').on('submit', '#form_project', function (e) {
			e.preventDefault();
			$('.btn-save').attr('disabled', 'disabled');
			$('.btn-save i').removeClass('hidden');
			var formData = new FormData($(this)[0]);
			$.ajax({
				url : "{{ route('cms.save_project_details') }}",
				type : 'POST',
				data : formData,
				processData : false,
				contentType : false,
				success 	: function (res) {
					console.log(res);
					$('#modal-default').modal('hide');
				}
			});
		});
		var search_data = '';
		$('body').on('submit', '#js-search', function (e) {
			e.preventDefault();
			search_data = $('#input_search').val();
			getPosts(1);
		});
	</script>
@endsection