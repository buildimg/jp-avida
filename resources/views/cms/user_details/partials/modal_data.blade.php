
<div class="modal fade " id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content modal-md">
            <form id="form_user_data">
                {{csrf_field()}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">
                        Change User Details
                    </h4>
                </div>
                <div class="modal-body" style="padding: 10px">
                    
                        <div class="form-group">
                            <label for="">Name</label>
                            <input type="text" class="form-control" id="input_name" name="input_name" value="{{ Auth::user()->name }}">
                            <div class="help-block text-red" id="err-input_name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Old Password</label>
                            <input type="password" class="form-control" id="input_old_password" name="input_old_password">
                            <div class="help-block text-red" id="err-input_old_password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">New Password</label>
                            <input type="password" class="form-control" id="input_new_password" name="input_new_password">
                            <div class="help-block text-red" id="err-input_new_password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Confirm Password</label>
                            <input type="password" class="form-control" id="input_new_password_confirmation" name="input_new_password_confirmation">
                            <div class="help-block text-red" id="err-confirm">
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <span class="pull-left text-red" id="js-msg"></span>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-circle-o-notch fa-spin hidden"></i> Save changes</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>