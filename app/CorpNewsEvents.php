<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CorpNewsEvents extends Model
{
    protected $connection = 'mysql_corp';
    protected $table = 'articles';
    public $timestamps = false;

    public function article_galleries ()
    {
        return $this->hasMany(CorpNewsEventsGallery::class, 'article_ID', 'article_ID');
    }
}
