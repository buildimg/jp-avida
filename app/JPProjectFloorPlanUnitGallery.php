<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JPProjectFloorPlanUnitGallery extends Model
{
    protected $table = 'jp_project_floor_plan_unit_galleries';
}
