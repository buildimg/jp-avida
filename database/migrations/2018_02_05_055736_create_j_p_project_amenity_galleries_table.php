<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJPProjectAmenityGalleriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jp_project_amenity_galleries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('gallery_title', 100)->nullable();
            $table->integer('corp_gallery_id')->nullable();
            $table->integer('corp_amenity_id')->nullable();
            $table->tinyInteger('status')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jp_project_amenity_galleries');
    }
}
