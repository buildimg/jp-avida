<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJPProjectNeighborhoodAttractionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jp_project_neighborhood_attractions', function (Blueprint $table) {
            $table->increments('id');
            $table->text('content')->nullable();
            $table->integer('corp_att_id')->nullable();
            $table->integer('corp_nh_id')->nullable();
            $table->tinyInteger('status')->default('1');  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jp_project_neighborhood_attractions');
    }
}
