<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\CorpProject;
use \App\JPProject;

class ProjectController extends Controller
{
	public function project($projectname)
	{
		$CorpProject = CorpProject::with([
			'amenity', 
			'amenity.amenity_gallery', 
			'concept', 
			'concept.concept_gallery', 
			'fp_overall', 
			'fp_overall.fp_overall_images', 
			'fp_unit', 
			'fp_unit.unit_images', 
			'location', 
			'constructions', 
			'constructions.construction_galleries', 
			'neighborhoods', 
			'neighborhoods.neighborhood_attractions'
		])
		->where('project_url_name', $projectname)
		->first();
		if (!$CorpProject) 
		{	
			return redirect('/');
		}
		$JPProject = JPProject::with([
			'concept', 
			'location', 
			'amenity', 
			'amenity.amenity_gallery',
			'fp_overall',
			'fp_overall.unit_images',
			'fp_unit',
			'fp_unit.unit_images',
			// 'constructions',
			// 'constructions.construction_gallery'
		])
		->where('corp_project_id', $CorpProject->project_ID)
		->first();
		
		return view('project', compact('CorpProject', 'JPProject'));
	} 
}
