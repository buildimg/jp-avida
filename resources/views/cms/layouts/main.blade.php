@include('cms.layouts.header')
@include('cms.layouts.sidebar')

@yield('content')

@include('cms.layouts.footer')