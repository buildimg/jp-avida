<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CorpProjectConstructionGallery extends Model
{
    protected $connection = 'mysql_corp';
    protected $table = 'project_construction_img';
    public $timestamps = false;
}
