<aside class="main-sidebar">
    <section class="sidebar">        
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
			<li class="{{ Request::is('cms/projects') ? 'active' : '' }}">
				<a href="{{ route('cms.projects') }}">
					<i class="fa fa-circle-o text-red"></i>
					<span>Projects</span>
				</a>
			</li>   
			{{--  <li>
				<a href="#">
					<i class="fa fa-circle-o text-yellow"></i>
					<span>Inquiries</span>
				</a>
			</li>  --}}
        </ul>
    </section>
</aside>