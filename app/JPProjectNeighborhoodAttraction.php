<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JPProjectNeighborhoodAttraction extends Model
{
    protected $table = 'jp_project_neighborhood_attractions';
}
