<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\CorpProject;
use \App\CorpHomeCarousel;
use \App\JPProject;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $CorpNewsEventsProperty = \App\CorpNewsEventsProperty::with(['articles', 'articles.article_galleries'])->get();

		$selected_jp_projects = [];
		$JPProject = JPProject::where('featured', 1)->where('status', 1)->select(['corp_project_id', 'project_name'])->take(3)->get();
		foreach ($JPProject as $proj)
		{
			$selected_jp_projects[] = $proj->corp_project_id;
		}
        $CorpProject = CorpProject::whereIn('project_ID', $selected_jp_projects)->inRandomOrder()->get();
        $HomeCarousel = \App\CorpHomeCarousel::orderBy('carousel_sorting_order', 'ASC')->get();
        return view('home', compact('CorpProject', 'JPProject', 'HomeCarousel'));
    }
}