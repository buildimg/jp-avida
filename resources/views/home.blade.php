@extends('layouts.main')
@section('styles')
    <link  href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
    <style>
        body{
            padding-top: 82px;
        }
        .search-bar {
            display: none;
            position: absolute;
            top: 81px;
            left: 0;
            width: 100%;
        }
    </style>
@endsection
@section('content')
        <div class="fotorama"
            data-loop="true"
            data-autoplay="true"
            data-width="100%"
            data-arrows="false"
            data-swipe="false"
            data-click="false"
            data-nav="false"
            data-stopautoplayontouch="false"
            data-transition="crossfade"
            data-transition-duration="999">
            @if($HomeCarousel)
                @foreach ($HomeCarousel as $item)
                    <img src="{{ 'https://www.' . $item->carousel_asset_URL . $item->carousel_image }}" alt="{{ $item->carousel_name }}">
                @endforeach
            @endif
        </div>

        {{-- @foreach($HomeCarousel as $item)
              <pre>{{ json_encode($item, JSON_PRETTY_PRINT)}}</pre> 
        @endforeach --}}

        <div class="avida-featured">
            <div class="container">
                <div class="text-center">
                    <h3 class="m-y-6"><strong>物件をお探しの方に</strong></h3>
                </div>
                <div class="row">
                    @foreach ($CorpProject as $proj)
                        <?php
                            $proj_jp = $JPProject->firstWhere('corp_project_id', $proj->project_ID);
                        ?>
                        {{--  {{ json_encode($proj_jp) }}  --}}
                        <div class="col-xs-6 col-sm-6 col-md-4 col-xs-block">
                            <div class="property">
                                <a href="{{ route('project', $proj->project_url_name) }}">
                                    <img src="{{ env('AVIDA_URL_PROJECT_UPLOADS') . $proj->project_upload_loc . $proj->project_thumb }}" class="w-100">
                                </a>
                                <h5 class="text-uppercase text-muted small">
                                    {{ App\CorpProject::PROJECT_TYPE_JAPANESE[$proj->project_type] }}
                                </h5>
                                <h3 class="property-project-name m-y-1">
                                    <a href="{{ route('project', $proj->project_url_name) }}"><strong>{{ $proj_jp ? $proj_jp->project_name ? $proj_jp->project_name : $proj->project_name : $proj->project_name }}</strong></a>
                                </h3>
                                <p class="property-address text-muted m-b-4">
                                    @if($proj->project_province == 'Metro Manila') 
                                        {{ $proj->project_address.', '.$proj->project_cities }}
                                    @else
                                        {{ $proj->project_address.', '.$proj->project_cities.', '.$proj->project_province }}
                                    @endif
                                </p>
                                <div class="property-details">
                                    <div class="row row-gap">
                                        <div class="col-xs-6 col-gap">
                                            <div class="small">
                                                @if(!empty($proj->project_unit_min) || !empty($proj->project_unit_max))
                                                    <span class="text-muted">ユニット面積</span>
                                                    <div>{{ $proj->project_unit_min.' - '.$proj->project_unit_max }} sq.m</div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-gap">
                                            <div class="small">
                                                @if(!empty($proj->project_price_min) || !empty($proj->project_price_max))
                                                    <span class="text-muted">価格帯</span>
                                                    <div>{{ $proj->project_price_min.'M - '.$proj->project_price_max.' M' }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="text-center p-y-6">
                    <a href="{{ route('properties') }}" class="btn btn-red btn-lg">View All</a>
                </div>
            </div>
        </div>
        <div class="avida-blog" style="display: block; background-image: url(https://www.avidaland.com/uploads/pop-assets/pop-bg-image-20170714095412.jpg)">
            <div class="avida-blog-block">
                <div class="container">
                      <div class="row">
                          <div class="col-md-8 col-md-offset-2">
                              <div class="avida-blog-lead text-center">
                                    <img src="{{ asset('images/pop-logo.png') }}" class="center-block img-responsive m-y-6" alt="POP Logo">
                              </div>
                              <div class="avida-blog-content">
                                <p class="white-text-shadow">Pursuit​ ​of​ ​Passion​ ​is​ ​your​ ​guide​ ​and​ ​inspiration​ ​in​ ​your​ ​journey​ ​of​ ​living​ ​a​ ​meaningful​ ​and balanced​ ​life.​ ​These​ ​stories​ ​on​ ​people​ ​pursuing​ ​their​ ​passions,​ ​on​ ​places​ ​to​ ​broaden​ ​your​ ​mind, and​ ​on​ ​different​ ​lifestyle​ ​interests​ ​are​ ​here​ ​to​ ​inform​ ​and​ ​inspire​ ​you​ ​to​ ​live​ ​well.</p>
                              </div>
                          </div>
                      </div>
                      <div class="text-center m-y-6">
                          <a href="http://www.pursuitofpassion.ph" target="_blank" class="btn btn-red btn-lg">View Blog</a>
                      </div>
                </div>
            </div>
        </div>
        <div class="about-avida" style="background-image: url('https://avidaland.com/uploads/aboutus-assets/about-us-bg-image-20170714034755.jpg')">
            <div class="container">
              <div class="row">
                  <div class="col-lg-10 col-lg-offset-1">
                        <div class="row">
                            <div class="col-md-4">
                                <h2 class="m-b-3 text-center"><strong>アビダランドについて</strong></h2>
                            </div>
                            <div class="col-md-8">
                                <div class="discover">
                                    <p><strong>楽しく、刺激に満ちた生活はアビダで始まります。簡単で、独特で、充実したライフスタイルをご経験ください。手に入るのは、生活に必要なものに留まりません。毎日、人生をお楽しみください!</strong></p>
                                    <p>アビダは、安全なコミュニティーに、よく考えられて設計された住宅をお届けします。アビダは、アクセスの良さ、独特な特徴と設備、確かな品質、楽に買物ができ、くつろげる住環境という五大要素が融合したマンション、戸建て、区画物件を扱い、全国で取扱物件を増やし続け、成長著しい中間層から支持されるデベロッパーになりました。</p>
                                    <div class="text-right m-y-5">
                                        <a href="{{ route('brandhistory') }}" class="discover-link"><strong>アビダランドについて</strong></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
    <script>
        var $document = $(document),
        $element = $('.navbar-main .search-bar'),
        className = 'hasScrolledNav';

        $document.scroll(function() {
            if ($document.scrollTop() > 400) {
                $element.fadeIn(300);
            } else {
                $element.fadeOut(300);
            }
        });
    </script>
@endsection
