<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CorpProjectFloorPlanOverall extends Model
{
    protected $connection = 'mysql_corp';
    protected $table = 'project_fp_overall';
    public $timestamps = false;
    public function fp_overall_images ()
    {
        return $this->hasMany(CorpProjectFloorPlanOverallImages::class, 'FPoverall_ID', 'FPoverall_ID');
    }
}
