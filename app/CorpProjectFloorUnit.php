<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CorpProjectFloorUnit extends Model
{
    protected $connection = 'mysql_corp';
    protected $table = 'project_fp_units';
    public $timestamps = false;

    public function unit_images ()
    {
        return $this->hasMany(CorpProjectFloorUnitGallery::class, 'FPunits_ID', 'FPunits_ID');
    }
}
