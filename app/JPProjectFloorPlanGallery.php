<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JPProjectFloorPlanGallery extends Model
{
    protected $table = 'jp_project_floor_plan_galleries';
}
