<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JPProjectConstructionGallery extends Model
{
    protected $table = 'jp_project_construction_galleries';
}
