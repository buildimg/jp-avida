<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JPProjectLocation extends Model
{
    protected $table = 'jp_project_locations';
}
