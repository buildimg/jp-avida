<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JPProject extends Model
{
    protected $table = 'jp_projects';
    
    public function amenity ()
    {
        return $this->hasOne(JPProjectAmenity::class, 'corp_project_id', 'corp_project_id');
    }

    public function concept ()
    {
        return $this->hasOne(JPProjectConcept::class, 'corp_project_id', 'corp_project_id');
    }

    public function fp_unit ()
    {
        return $this->hasOne(JPProjectFloorPlanUnit::class, 'corp_project_id', 'corp_project_id');
    }
    
    public function fp_overall ()
    {
        return $this->hasOne(JPProjectFloorPlan::class, 'corp_project_id', 'corp_project_id');
    }

    public function location ()
    {
        return $this->hasOne(JPProjectLocation::class, 'corp_project_id', 'corp_project_id');
    }

    public function constructions ()
    {
        return $this->hasOne(JPProjectConstruction::class, 'corp_project_id', 'corp_project_id');
    }
    
    // public function neighborhoods ()
    // {
    //     return $this->hasOne(CorpProjectNeighborhood::class, 'nh_ID', 'project_neighborhood');
    // }
}
