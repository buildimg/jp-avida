<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CorpHomeCarousel extends Model
{
    protected $connection = 'mysql_corp';
    protected $table = 'carousel_main';
    public $timestamps = false;
}
