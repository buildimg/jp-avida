<div class="nav-static">
	<ul class="nav list-unstyled">
	    <li><a class="{{ Request::is('brand-history') ? 'active' : '' }}" href="{{ route('brandhistory') }}">会社紹介</a></li></li>
	    <li><a class="{{ Request::is('mission-and-vision') ? 'active' : '' }}" href="{{ route('missionandvision') }}">ミッションとビジョン</a></li></li>
	</ul>
</div>